import { AsyncStorage } from "react-native";
import SoundPlayer from 'react-native-sound-player';

export const isSignedIn = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('user_details').then((data)=> {
            if (data !== null) {
                resolve(true);
            } else {
                resolve(false);
            }
        })
            .catch(err => reject(err));
    });
};


getMusicStatus = async () =>{
    var musicStatus = await AsyncStorage.getItem('isMusic')
    console.log('--------------------')
    console.log(musicStatus)
if (musicStatus === '1'){
    SoundPlayer.resume()
 }
  
}

