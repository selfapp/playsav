import React from 'react';
import {Image, Dimensions, Platform, AsyncStorage} from 'react-native';
import {StackNavigator, TabNavigator, TabBarBottom} from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import LogIn from './Screens/log_in';
import SignUp from './Screens/sign_up';
import Interest from './Screens/interest_page';
import Dashboard from './Screens/dashboard';
import Savings from './Screens/savings';
import Coupons from './Screens/coupons';
import CouponDetail from './Screens/coupon_detail';
import Spotlight from './Screens/spotlight';
import Games from './Screens/games';
import GameTypes from './Screens/game_types';
import Location from './Screens/location';
import MyList from './Screens/my_list';
import RedeemedCoupon from './Screens/redeemed_coupon';
import MyListCoupons from './Screens/my_list_coupons';
import SpotlightCouponDetail from './Screens/spotlight_coupon_detail';
import SoundPlayer from 'react-native-sound-player';
//import { isMusic } from './auth';

const iphoneHeight = Dimensions.get('window').height

export const CouponStackNavigator = StackNavigator({
    Coupons: Coupons,
    CouponDetail: CouponDetail,
    RedeemedCoupon: RedeemedCoupon
}, {
    headerMode: 'none'
});

export const MyListStackNavigator = StackNavigator({
    MyList: MyList,
    MyListCoupons: MyListCoupons,
    CouponDetail: CouponDetail,
}, {
    headerMode: 'none'
});

// export const SpotlightStackNavigator = StackNavigator({
//     Spotlight: Spotlight,
//     SpotlightCouponDetail: SpotlightCouponDetail,
// }, {
//     headerMode: 'none'
// });
export const GameStackNavigator = StackNavigator({
    GameTypes: GameTypes
}, {
    headerMode: 'none'
});

export const SignedInTabNavigator = TabNavigator({
    CouponsTab: {
        screen: ({navigation}) => <CouponStackNavigator screenProps={{SignedOutNavigation: navigation}}/>,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={require('./Assets/coupon-nav.png')}
                    style={{tintColor: tintColor}}
                />
            )
        }
    },
    // Spotlight: {
    //     screen: ({navigation}) => <SpotlightStackNavigator screenProps={{SignedOutNavigation: navigation}}/>, navigationOptions: {
    //         tabBarIcon: ({tintColor}) => (
    //             <Image
    //                 source={require('./Assets/spot-ligh-nav.png')}
    //                 style={{tintColor: tintColor}}
    //             />
    //         )
    //     }
    // },
    GameTypes: {
        screen: ({navigation}) => <GameStackNavigator
            screenProps={{SignedOutNavigation: navigation, SignedInStackNavigator: navigation}}/>, navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={require('./Assets/game-nav.png')}
                    style={{tintColor: tintColor}}
                />
            )
        }
    },

    Location: {
        screen: Location,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={require('./Assets/location-nav.png')}
                    style={{tintColor: tintColor}}
                />
            )
        }
    },
    MyList: {
        screen: ({navigation}) => <MyListStackNavigator screenProps={{SignedOutNavigation: navigation}}/>,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={require('./Assets/my-list.png')}
                    style={{tintColor: tintColor}}
                />
            )
        }
    }

}, {
    tabBarComponent: ({jumpToIndex, ...props}) => {
        return (
            <LinearGradient start={{x: 1, y: 0}} colors={['#FBFCFF', '#C9C7CA']}>
                <TabBarBottom {...props} jumpToIndex={jumpToIndex}/>
            </LinearGradient>
        )

    }
    ,
    animationEnabled: true,
    swipeEnabled: false,
    tabBarPosition: 'bottom',
    tabBarOptions: {
        showIcon: true,
        showLabel: false,
        iconStyle: {
            width: 30,
            height: 30,
        },

        // labelStyle: {
        //     fontSize: 17, paddingBottom: 10
        // },
        activeTintColor: '#4191AA',

        tabStyle: {
            alignSelf: 'center',
            padding: ((Platform.OS === 'ios' && iphoneHeight === 812) ? 5 : 0),
            margin: ((Platform.OS === 'ios' && iphoneHeight === 812) ? 20 : 0),
        },
        style: {backgroundColor: 'transparent', height: ((Platform.OS === 'ios' && iphoneHeight === 812) ? 20 : 55)}
    },
    headerMode: 'none'
});


export const SignedInStackNavigator = StackNavigator({
    Dashboard: Dashboard,
    Games: Games,
    //GameTypes: GameTypes,
    Tabs: SignedInTabNavigator,
    Savings: Savings,
    RedeemedCoupon: RedeemedCoupon,
    InterestSlides: Interest,

}, {
    headerMode: 'none'
});

export const SignedOutStackNavigator = StackNavigator({
    LogIn: LogIn,
    InterestSlides: Interest,
    SignUp: SignUp
}, {
    headerMode: 'none'
});


export const createRootNavigator = (signedIn = false) => {
    return StackNavigator(
        {
            SignedIn: {
                screen: ({navigation}) => <SignedInStackNavigator screenProps={{rootNavigation: navigation}}/>,
                navigationOptions: {
                    gesturesEnabled: false
                }
            },
            SignedOut: {
                screen: ({navigation}) => <SignedOutStackNavigator screenProps={{rootNavigation: navigation}}/>,
                navigationOptions: {
                    gesturesEnabled: false
                }
            },
        },
        {
            headerMode: "none",
            initialRouteName: signedIn === false ? "SignedOut" : "SignedIn"
        }
    );
};

