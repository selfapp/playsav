var baseURL = 'http://18.217.10.138/api/'; // dev
import { AsyncStorage} from 'react-native';

var api = {
    request(url, method, body) {
        return AsyncStorage.getItem('access_token').then((data)=> {
            let access_token = data
            console.log('Url ---',baseURL + url)
            return fetch(baseURL + url, {
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ (access_token ? access_token : null)
                },
                body: body === null ? null : JSON.stringify(body)
            })
        });
},
    // request(url, method, body, header) {
    //     return fetch(baseURL+url, {
    //         method: method,
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'Authorization': header
    //         },
    //         body: JSON.stringify(body)
    //     })
    // },
    getRequestApi(url) {

        return AsyncStorage.getItem('access_token').then((data)=> {
            let access_token = data
        return fetch(baseURL + url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ (access_token ? access_token : null)
            }
        })
    })},


    getRequest(url, method) {
        return fetch(url, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': null
            }
        })
    },


    delRequestApi(url) {
console.log(url)
        return AsyncStorage.getItem('access_token').then((data)=> {
            let access_token = data
        return fetch(baseURL + url, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ (access_token ? access_token : null)
            }
        })
    })},
};

module.exports = api;