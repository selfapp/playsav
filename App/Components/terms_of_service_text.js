import React from 'react';
import {Text, Linking} from 'react-native';


export default TermsofServiceText = (props) => {
    return (
        <Text style={{fontSize: 14, color: '#6A6A6A', marginTop: 20, lineHeight: 20}}>By Creating an
            account, you are agreeing with our
            <Text style={{fontWeight: '600', color: '#000000', textDecorationLine: 'underline'}}
            onPress={() => {

                Linking.canOpenURL('https://playsav.com/mobile-terms/').then(supported => {
                    if (supported) {
                      Linking.openURL('https://playsav.com/mobile-terms/');
                    } else {
                      console.log("Don't know how to open URI: " + this.props.url);
                    }
                  });              
            }}
            >
                {' '}Terms of Service </Text>
            and confirming that you are
            <Text style={{fontWeight: '600', color: '#000000'}}> at least 13 years
                old.</Text>
        </Text>
    )
}