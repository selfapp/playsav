import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


export default GradientSilverBox = (props) => {
    return (
        <LinearGradient colors={['white', 'silver']} style={[styles.gradientSilverBox, {height: props.height || Dimensions.get('window').height*0.1}]}>
            {props.children}
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    gradientSilverBox: {
        // paddingVertical: 10,
        alignItems: 'center', justifyContent: 'center',
        borderRadius: 4,
    }
});