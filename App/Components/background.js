import React from 'react';
import {View, ScrollView, ImageBackground} from 'react-native'
import Header from '../Components/header';
import LinearGradient from 'react-native-linear-gradient';


export default Background = (props) => {
    const color = props.backgroundColor || ['#336979', '#8DA9B7'];
    const header = props.withoutHeader ? null : <Header {...props}/>
    return (
        <View style={{flex: 1}}>
        {header}
        {
            props.useBackGroundImage ? (
                <ImageBackground style={{
                    flex: 1,
                    alignItems: 'center',  
                }} source={require('../Assets/bgcopy.png')} resizeMode='stretch' >
                    <ScrollView contentContainerStyle={{alignItems: 'center', paddingBottom: 50}}
                                scrollEventThrottle={1000}
                                showsVerticalScrollIndicator={false}
                                keyboardShouldPersistTaps={'handled'}
                                onScroll={props.handleScroll}>
                        {props.children}
                    </ScrollView>
                </ImageBackground>
            ):(
props.withoutSroll ? 
<LinearGradient colors={color} style={{flex: 1}}>
{props.children}
</LinearGradient>
:

             <LinearGradient colors={color} style={{flex: 1}}> 
                <ScrollView contentContainerStyle={{alignItems: 'center', paddingBottom: 50}}
                            scrollEventThrottle={1000}
                            showsVerticalScrollIndicator={false}
                            keyboardShouldPersistTaps={'handled'}
                            onScroll={props.handleScroll}>
                    {props.children}
                </ScrollView>
             </LinearGradient>
            )
           
        }
       
        </View>
    )
}