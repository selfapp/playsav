import React from 'react';
import {Text} from 'react-native';
import navigate from '../Components/navigate';

export default AuthFooter = (props) => {
    if (props.signUp) {
        return(
            <Text style={{fontSize: 16, color: '#FFFFFF', marginTop: 60}}>Already member?
                <Text style={{fontWeight: '800'}} onPress={() => navigate.navigateTo(props.navigation, 'LogIn')}> Login</Text> now
            </Text>
        )
    } else {
        return(
            <Text style={{fontSize: 16, color: '#FFFFFF', marginTop:30}}>Not a member yet?
                <Text style={{fontWeight: '800'}} onPress={() => navigate.navigateTo(props.navigation, 'SignUp')}> Signup</Text> now
            </Text>
        )
    }
}