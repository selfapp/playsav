import React from 'react';
import {ActivityIndicator,StyleSheet,View} from 'react-native';
import Dimensions from 'Dimensions';

export default Loader = (props) => {
return (
    <View style={styles.loader}>
                        <ActivityIndicator size="large" color='rgb(54,122,223)' />
    </View>
)
}
const styles = StyleSheet.create({
    loader: {
        backgroundColor: 'rgba(245,245,245, 0.7)',
        height: Dimensions.get('window').height,
       // flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignSelf: 'center',
        justifyContent: 'center'
    }
});