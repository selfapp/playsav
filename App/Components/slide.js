import React, {Component} from 'react';
import {View, TouchableWithoutFeedback, Text} from 'react-native';
import * as Animatable from 'react-native-animatable';
import TextBox from '../Components/text_box';
import SliderDots from '../Components/slider_dots';


export class Slider extends Component {

    animate() {
        try{  
            this.refs.first.zoomIn();
            this.refs.second.zoomIn();
            this.refs.third.zoomIn();
        }catch(e){}
    }

    componentDidMount() {
        this.animate()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.title !== nextProps.title) {
            this.animate()
        }
    }

    render() {
        const {selected, handleState, page, title, list} = this.props;
        return(
            <View style={{width: '100%'}}>
                <Text style={{alignSelf: 'center', fontSize: 20, color: '#5B5B5B', marginVertical: 15}}>{title}</Text>
                <TouchableWithoutFeedback onPress={() => {
                    handleState(list[0])
                }}>
                    <Animatable.View useNativeDriver ref={'first'}>
                        <TextBox checkboxList title={list[0]} selected={selected[page]}/>
                    </Animatable.View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    handleState(list[1])
                }}>
                    <Animatable.View useNativeDriver ref={'second'}>
                        <TextBox checkboxList title={list[1]} selected={selected[page]}/>
                    </Animatable.View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    handleState(list[2])
                }}>
                    <Animatable.View useNativeDriver ref={'third'}>
                        <TextBox checkboxList title={list[2]} selected={selected[page]}/>
                    </Animatable.View>
                </TouchableWithoutFeedback>
                {/*<SliderDots page={page} size={12}/>*/}
            </View>
        )
    }
}

export default Slide = (props) => {
    // const {page} = props;
    // if (page === 1) return <Slider {...props} title={'ENTERTAINMENT'} list={['Movies', 'Television', 'Performances']}/>;
    // else if (page === 2) return <Slider {...props} title={'SPORTS'} list={['Cricket', 'Football', 'Hockey']}/>;
    // else if (page === 3) return <Slider {...props} title={'PROFESSION'} list={['Job', 'Businessman', 'Service']}/>;
    return <Slider {...props} title={'ENTERTAINMENT'} list={['Movies', 'Television', 'Performances']}/>;

}