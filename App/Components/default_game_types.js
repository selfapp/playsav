const DefaultGameTypes = [
        {
            'title': "Food / Drinks / Grocery",
            'game_key': "food_drink_grocery",
            'id':'food',
            'background_img': require("../Assets/1.png"),
            'icon_img': require("../Assets/FDG.png")
        },
        {
            'title': "Restaurants & Entertainment",
            'game_key': "restaurants_entertainment",
            'id':'restaurants',
            'background_img': require("../Assets/2.png"),
            'icon_img': require("../Assets/Rest.png")
        },
        {
            'title': "Clothing & Apparel",
            'game_key': "clothing_Apparel",
            'id':'clothing',
            'background_img': require("../Assets/3.png"),
            'icon_img': require("../Assets/Clothing.png")
        },
        {
            'title': "Sporting Goods / Outdoors",
            'game_key': "sporting_goods_outdoors",
            'id':'sporting',
            'background_img': require("../Assets/4.png"),
            'icon_img': require("../Assets/Sport.png")
        },
        {
            'title': "Pets",
            'game_key': "pets",
            'id':'pets',
            'background_img': require("../Assets/5.png"),
            'icon_img': require("../Assets/Pets.png")
        },
        {
            'title': "Home & Garden",
            'game_key': "home_garden",
            'id':'home',
            'background_img': require("../Assets/6.png"),
            'icon_img': require("../Assets/HomeIcon.png")
        },
        {
            'title': "Personal Care",
            'game_key': "personal_care",
            'id':'personal',
            'background_img': require("../Assets/7.png"),
            'icon_img': require("../Assets/P_Care.png")
        },
        {
            'title': "Travel",
            'game_key': "travel",
            'id':'travel',
            'background_img': require("../Assets/8.png"),
            'icon_img': require("../Assets/Travel.png")
        },
        {
            'title': "Electronics & Office",
            'game_key': "electronics_office",
            'id':'electronics',
            'background_img': require("../Assets/9.png"),
            'icon_img': require("../Assets/Office.png")
        },
        {
            'title': "Auto",
            'game_key': "auto",
            'id':'auto',
            'background_img': require("../Assets/10.png"),
            'icon_img': require("../Assets/Auto.png")
        },
        {
            'title': "Baby & Toddler",
            'game_key': "baby_toddler",
            'id':'baby',
            'background_img': require("../Assets/11.png"),
            'icon_img': require("../Assets/baby.png")
        },
        {
            'title': "Services",
            'game_key': "services",
            'id':'services',
            'background_img': require("../Assets/12.png"),
            'icon_img': require("../Assets/service.png")
        },
        {
            'title': "Toys",
            'game_key': "toys",
            'id':'toys',
            'background_img': require("../Assets/13.png"),
            'icon_img': require("../Assets/toys.png")
        },
        {
            'title': "Books/Music/Media",
            'game_key': "books_music_media",
            'id':'books',
            'background_img': require("../Assets/14.png"),
            'icon_img': require("../Assets/books.png")
        },
        {
            'title': "Flowers & Gifts",
            'game_key': "flowers_gifts",
            'id':'flowers',
            'background_img': require("../Assets/15.png"),
            'icon_img': require("../Assets/Gift.png")
        },
        {
            'title': "Jewellery & Accesories",
            'game_key': "jwellery_accesories", 
            'id':'jewelry', 
            'background_img': require("../Assets/16.png"),
            'icon_img': require("../Assets/Jewelry.png")
        },
];

export default DefaultGameTypes;