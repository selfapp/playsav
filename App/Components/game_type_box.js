import React from 'react';
import {View, Text, StyleSheet,Dimensions, Platform, ImageBackground, FlatList, TouchableOpacity, Image} from 'react-native'
const { width, height } = Dimensions.get('window')

export default GameTypeBox = (props) => {

let gameTypeData = {
    '1':require("../Assets/1.png"),
    'FDG':require("../Assets/FDG.png"),
    '2':require("../Assets/2.png"),
    'Rest':require("../Assets/Rest.png"),
    '3':require("../Assets/3.png"),
    'Clothing': require("../Assets/Clothing.png"),
    '4': require("../Assets/4.png"),
    'Sport': require("../Assets/Sport.png"),
    '5': require("../Assets/5.png"),
    'Pets': require("../Assets/Pets.png"),
    '6': require("../Assets/6.png"),
    'HomeIcon': require("../Assets/HomeIcon.png"),
    '7': require("../Assets/7.png"),
    'P_Care': require("../Assets/P_Care.png"),
    '8': require("../Assets/8.png"),
    'Travel': require("../Assets/Travel.png"),
    '9': require("../Assets/9.png"),
    'Office': require("../Assets/Office.png"),
    '10': require("../Assets/10.png"),
    'Auto': require("../Assets/Auto.png"),
    '11': require("../Assets/11.png"),
    'baby': require("../Assets/baby.png"),
    '12': require("../Assets/12.png"),
    'service': require("../Assets/service.png"),
    '13': require("../Assets/13.png"),
    'toys': require("../Assets/toys.png"),
    '14': require("../Assets/14.png"),
    'books': require("../Assets/books.png"),
    '15': require("../Assets/15.png"),
    'Gift': require("../Assets/Gift.png"),
    '16': require("../Assets/16.png"),
    'Jewelry': require("../Assets/Jewelry.png")
}



    return (
            <FlatList
                style={{flex:1, flexDirection:'row'}}
                data={props.gameTypes}   
                // extraData={props.selectedGameType}
                renderItem={({ item }) =>  
                <ImageBackground source={gameTypeData[item.background_img]}    
                resizeMode='stretch'                           
                keyExtractor={(item, index) => index.toString()}
                style={{flex:1,marginVertical:10,paddingVertical:10,height:height/6 + 15,width:width,
                    justifyContent: 'center',alignItems:'center',flexDirection:'row',
                }}>     
                    <TouchableOpacity
                    style={{flexDirection:'row'}}
                  
                    onPress={() => props.getSelectedGameType(item.ids, item.count)}>

                        <Image 
                        resizeMode='contain'
                               source={gameTypeData[item.icon_img]}
                        />
                        <View style={styles.TextComponentStyleGrid}>
                            <Text style={styles.ItemTextStyle}>{item.title}</Text>
                        </View>
                    </TouchableOpacity>
                </ImageBackground>
                    }
            />
    )
}

const styles = StyleSheet.create({
    ImageComponentStyleGrid: {
        justifyContent: 'center',
        flex: 0.2,
        alignItems: 'center',
        flexDirection:'row'
    },
    TextComponentStyleGrid: {
        flexWrap: 'wrap',
        flex:0.8,
        justifyContent: 'center',
        paddingTop:5,
    },
    ItemTextStyle: {
        color: 'white',
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center',
    }
});