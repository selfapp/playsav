import React from 'react';
import {View, StyleSheet} from 'react-native';

export default SliderDots = (props) => {
    return (
        <View style={[styles.view, props.containerStyle]}>
            <View style={[styles.dot, {height: props.size, width: props.size, borderRadius: (props.size/2)}, props.page === 1 ? [styles.activeDot, {borderWidth: props.borderWidth || 2}] : null]}/>
            <View style={[styles.dot, {height: props.size, width: props.size, borderRadius: (props.size/2)}, props.page === 2 ? [styles.activeDot, {borderWidth: props.borderWidth || 2}] : null]}/>
            <View style={[styles.dot, {height: props.size, width: props.size, borderRadius: (props.size/2)}, props.page === 3 ? [styles.activeDot, {borderWidth: props.borderWidth || 2}] : null]}/>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {marginTop: 30, flexDirection: 'row', width: 70, alignSelf: 'center', justifyContent: 'space-between'},
    dot: {backgroundColor: '#4191AA'},
    activeDot: {borderColor: '#4191AA', backgroundColor: '#ffffff'}
});