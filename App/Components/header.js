import React from 'react';
import {View, Image, Dimensions, Text, TouchableWithoutFeedback, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import navigate from '../Components/navigate';


const {width, height} = Dimensions.get('window');
export default header = (props) => {
    function leftIcon() {
        if (props.left) {
            return (
                <TouchableWithoutFeedback onPress={() =>
                {
                    props.leftNavigation ?  props.leftNavigation.goBack(null) : props.navigation.goBack(null)
                }
               }>
                    <View style={{
                        position: 'absolute',
                        left: 10,
                        top: 20,
                        height: 45,
                        width: 45,
                        zIndex: 10,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <Image source={props.left}/>
                    </View>
                </TouchableWithoutFeedback>
            )
        } else if (props.leftToDashboard) {
            return (
                <TouchableWithoutFeedback onPress={() => navigate.navigateWithReset(props.navigation, 'Dashboard')}>
                    <View style={{
                        position: 'absolute',
                        left: 10,
                        top: 20,
                        height: 45,
                        width: 45,
                        zIndex: 10,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <Image source={props.leftToDashboard}/>
                    </View>
                </TouchableWithoutFeedback>
            )
        } else {
            return null
        }
    }

function rightIcon(){
    if (props.home){
        return (
            <TouchableWithoutFeedback onPress={() => navigate.navigateWithReset(props.navigation, 'Dashboard')}>
                    <Image source={props.home} style={{position: 'absolute', right: 20, top: 30}}/>
            </TouchableWithoutFeedback>
        )
    }else if(props.settings){
        return (
            <TouchableWithoutFeedback style={{height:65,width:65}}
            onPress={()=>{props.openModal()}}
            >
                    <Image source={props.settings} style={{position: 'absolute', right: 20, top: 30}}/>
            </TouchableWithoutFeedback>
        )
    }else if(props.skip){
        return(
        <TouchableOpacity style={{height:65,width:65, marginLeft:width-80, alignItems:'flex-end'}}
            onPress={()=>{props.onPressSkip()}}
            >
                <View>
                    <Text style={{position: 'absolute', right: 20, top: 30, color: 'rgb(51,106,121)', padding: 10, fontSize: 16}}> {props.skip}</Text>
                </View>
            </TouchableOpacity>
        )
    } else{
        return (null)
    }
}

    return (
        <View>
            <LinearGradient colors={['silver', 'white']} style={{
                height: 100,
                shadowColor: 'rgba(0,0,0,0.44)',
                shadowOffset: {width: 3, height: 40, zIndex: 3},
                shadowOpacity: 1.0,
            }}>
                 {rightIcon()}
                {/* {props.right ? <Image source={props.right} style={{position: 'absolute', right: 20, top: 30}}/> : null} */}
                {leftIcon()}
            </LinearGradient>
            <Image source={require('../Assets/playsav-mini-logo.png')}
                   style={{position: 'absolute', top: width * 0.07, zIndex: 2, alignSelf: 'center'}}/>
            <View style={{
                backgroundColor:props.useBackGroundImage ? '#281C88' : '#336979',
                // backgroundColor: '#281C88',
                // backgroundColor: '#336979',

                height: width * 0.3,
                width: width * 0.3,
                borderRadius: width * 0.3 / 2,
                alignSelf: 'center',
                position: 'absolute',
                transform: [{scaleX: 4}],
                top: width * 0.15
            }}>
                {/* <View style={{
                    borderColor: 'lightgrey',
                    borderWidth: Platform.OS === 'ios' ? 1 : 0,
                    shadowColor: 'rgb(0,0,0)',
                    shadowOffset: {width: 0, height: 5},
                    shadowOpacity: 1.0,
                    elevation: 2,
                    borderRadius: width * 0.3 / 2,
                    height: width * 0.3,
                    width: width * 0.3
                }}/> */}
            </View>
        </View>
    )
};