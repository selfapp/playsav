import React from 'react';
import {Text} from 'react-native';

export default SilverBoxHeader = (props) => {
    if (props.interest) {
        return(
            <Text style={{fontSize: 16, color: '#000000', fontWeight: '600', marginVertical: 10, alignSelf: 'center'}}>
                Tell Us About Your Interest:
            </Text>
        )
    } else {
        return(
            <Text style={{fontSize: 16, color: '#000000', fontWeight: '600', marginVertical: 10, alignSelf: 'center'}}>
                To Get Started, Tell Us a Bit About Yourself:
            </Text>
        )
    }
}