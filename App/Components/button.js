import React from 'react';
import {Text, TouchableOpacity, Platform} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default Button = (props) => {
    return (
        <TouchableOpacity 
        disabled={props.disabled}
        onPress=
        {props.onPress}>
          <LinearGradient colors ={['#CDCDCD', '#CDCDCD']}
            style={{
                height: props.height || 50,
                paddingHorizontal: props.paddingHorizontal,
                opacity:props.disabled?0.5:1,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomColor: props.shadowColor || '#275979',
                borderBottomWidth: 3,
                
    
                // shadowColor: props.shadowColor || '#275979', shadowOffset: {width: 0, height: 6},
                // shadowOpacity: 0.5,
                // borderBottomColor: Platform.OS === 'android' ? '#275979' : null,
                // borderBottomWidth: Platform.OS === 'android' ? 2 : null,
            }}>
                <Text style={{fontSize: props.fontSize || 18, color: '#5F5F5F', fontWeight: '700'}}>{props.name}</Text>
            </LinearGradient>
        </TouchableOpacity>
    )
}