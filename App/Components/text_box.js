import React, {Component} from 'react';
import {View,
     Image,
     TextInput,
     StyleSheet,
     Text,
     Dimensions,
    TouchableWithoutFeedback
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import SubCategories from './subCategories';

export default class TextBox extends Component {
    render() {
        if (this.props.checkboxList) {
            const {item, selected, select, categoryHandler, sub} = this.props;
            return (
                <LinearGradient colors={selected ? ['#050007', '#4E829A'] : ['#D3D8D4', '#FEFEFE']}
                                style={[styles.textBox, {width: '100%', paddingHorizontal: 20}]}>
                    <TouchableWithoutFeedback onPress={() => categoryHandler(item)}>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            width: '100%',
                            height: 60,
                            alignItems: 'center'
                        }}>
                            <TouchableWithoutFeedback onPress={selected ? (() => select(item, sub, (selected.length > 0))) : () => categoryHandler(item)}>
                                <Image
                                    source={(selected && (selected.length > 0))  ? require('../Assets/check.png') : require('../Assets/uncheck.png')}/>
                            </TouchableWithoutFeedback>
                            <Text style={{
                                fontSize: Dimensions.get('window').width * 0.045, 
                                color: selected ? '#ffffff' : '#5B5B5B',
                                fontWeight: selected ? '900' : null,
                            }}>{item}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    {
                        selected ?
                            <View style={{marginBottom: 15, width: '100%', alignItems: 'center'}}>
                                {sub.map((subObj) => {
                                    return <SubCategories sub={subObj} selected={selected.indexOf(subObj) >= 0}
                                                          select={() => select(item, subObj)} key={subObj.name}/>
                                })}
                            </View> : null
                    }
                </LinearGradient>
            )
        } else if (this.props.game) {
            return (
                <LinearGradient colors={['#D3D8D4', '#FEFEFE']}
                                style={[styles.textBox, {
                                    flexDirection: 'row',
                                    width: '100%',
                                    height: 60,
                                    marginTop: 10,
                                    justifyContent: 'space-between',
                                    paddingHorizontal: 20
                                }]}>
                    <Text style={{fontSize: 18, color: '#5B5B5B'}}>{this.props.title}</Text>
                    <View style={{flexDirection: 'row'}}>
                        <Image source={require('../Assets/facebook.png')} style={{marginRight: 20}}/>
                        <Image source={require('../Assets/twitter.png')}/>
                    </View>
                </LinearGradient>
            )
        } else {
            const {icon, autoCapitalize, reference, returnKeyType, keyboardType, placeholder, secureTextEntry, onChangeText, onSubmitEditing, width} = this.props;
            return (
                <LinearGradient colors={['#D3D8D4', '#FEFEFE']}
                                style={[styles.textBox, {width: width || '100%', flexDirection: 'row'}]}>
                    <Image source={icon} style={{marginRight: 10}}/>
                    <TextInput 
                                allowFontScaling={false}
                                underlineColorAndroid='transparent'
                               autoCorrect={false}
                               ref={reference}
                               placeholderTextColor={'#5B5B5B'}
                               autoCapitalize={autoCapitalize || 'none'}
                               returnKeyType={returnKeyType || 'next'}
                               keyboardType={keyboardType}
                               placeholder={placeholder}
                               secureTextEntry={secureTextEntry}
                               onChangeText={onChangeText}
                               onSubmitEditing={onSubmitEditing}
                               style={{height: 50, flex: 1, fontSize: 18}}
                    />
                </LinearGradient>
            )
        }
    }
}

const styles = StyleSheet.create({
    textBox: {
        borderWidth: 1,
        borderColor: '#4191AA',
        alignItems: 'center',
        paddingLeft: 10,
        marginTop: 20
    }
});