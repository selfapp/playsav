import React from 'react';
import {View, StyleSheet, Platform} from 'react-native'


export default SilverBox = (props) => {
    let {
        style
    } = props;
    if(style == null || style === undefined) {
        style = {}
    }
    return (
        <View style={[styles.silverBox, {paddingBottom: props.paddingBottom || 65, paddingVertical: props.paddingVertical || 30}, style]}>
            {props.children}
        </View>
    )
}

const styles = StyleSheet.create({
    silverBox: {
        backgroundColor: '#E5E5E5', padding: 10, borderRadius: 4, marginTop: 20, shadowColor: 'rgba(75,76,77,0.75)',
        shadowOffset: {width: 0, height: 6}, shadowOpacity: 1.0, width: '90%',
        borderBottomColor: Platform.OS === 'android' ? 'rgba(75,76,77,0.75)' : null,
        borderBottomWidth: Platform.OS === 'android' ? 2 : null,
    }
});