import React, {Component} from 'react';
import {View, Image, Text, Dimensions, TouchableWithoutFeedback} from 'react-native';

class SubCategories extends Component {
    render() {
        const {selected, sub, select} = this.props;
        return (
            <TouchableWithoutFeedback onPress={select}>
                <View style={{
                    height: 40,
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    backgroundColor: selected ? 'silver' : 'transparent',
                    borderColor: 'silver',
                    borderWidth: 1,
                    width: '100%',
                    marginBottom: 10,
                    paddingHorizontal: 20,
                    borderRadius: 5
                }}>
                    <Image
                        source={selected ? require('../Assets/check.png') : require('../Assets/uncheck.png')}/>
                    <Text style={{
                        color: '#ffffff',
                        fontSize: Dimensions.get('window').width * 0.04
                    }}>{sub.name}</Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }
};


export default SubCategories;