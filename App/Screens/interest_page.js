import React, {Component} from 'react';
import {FlatList,InteractionManager, View} from 'react-native';
import Background from '../Components/background';
import SilverBox from '../Components/silver_box';
import SilverBoxHeader from '../Components/silverbox_header';
import Button from '../Components/button';
import navigate from '../Components/navigate';
import TextBox from '../Components/text_box';
import api from '../api';
import Loader from '../Components/activityIndicator'
import firebase from 'react-native-firebase';


const interestItem = [
    {  
      name: "Restaurants & Entertainment",
      id: 1,
      children: [{
          name: "Indoor/Outdoor Recreation",
          id: 17,
        },{
          name: "Movies & Performances",
          id: 18,
        },{
          name: "Restaurants ",
          id: 19,
        },{
          name: "Theme Parks",
          id: 20,
        }]
    },
    {
      name: "Clothing & Apparel ",
      id: 2,
      children: [{
          name: "Activewear",
          id: 21,
        },{
          name: "Children's",
          id: 22,
        },{
          name: "Men's",
          id: 23,
        },{
          name: "Women's",
          id: 24,
        }]
    },
    {
      name: "Sporting Goods/Outdoors",
      id: 3,
      children: [{
          name: "Activewear",
          id: 25,
        },{
          name: "Fishing/Hunting/Camping Gear",
          id: 26,
        },{
          name: "Sport & Exercise Equipment ",
          id: 27,
        },{
          name: "Watersports",
          id: 28,
        }]
    },
    {
        name: "Food/Drinks/Grocery ",
        id: 4,
        children: [{
            name: "Baking & Spices",
            id: 29,
          },{
            name: "Breakfast",
            id: 30,
          },{
            name: "Chips & Snacks",
            id: 31,
          },{
            name: "Condiments & Canned Goods",
            id: 32,
          },
          {
            name: "Dairy",
            id: 33,
          },
          {
            name: "Deli & Bakery",
            id: 34,
          },
          {
            name: "Drinks",
            id: 35,
          },
          {
            name: "Frozen Goods",
            id: 36,
          },
          {
            name: "Household Supplies",
            id: 37,
          },
          {
            name: "Paper & Plastic Goods",
            id: 38,
          },
          {
            name: "Produce",
            id: 39,
          },
          {
            name: "Meat",
            id: 40,
          },]
      },
      {
        name: "Pets",
        id: 5,
        children: [{
            name: "Cats",
            id: 41,
          },{
            name: "Dogs",
            id: 42,
          },{
            name: "Fish & Birds",
            id: 43,
          },{
            name: "Other",
            id: 44,
          }]
      },
      {
        name: "Home & Garden",
        id: 6,
        children: [{
            name: "Home Improvement",
            id: 45,
          },{
            name: "Indoor Living & Decor",
            id: 46,
          },{
            name: "Outdoor Living & Lawncare",
            id: 47,
          },{
            name: "Tools & Machinery",
            id: 48,
          }]
      },
      {
        name: "Personal Care",
        id: 7,
        children: [{
            name: "Feminine Products",
            id: 49,
          },{
            name: "Essentials",
            id: 50,
          },{
            name: "Makeup & Accessories",
            id: 51,
          },{
            name: "Medicine & Supplements",
            id: 52,
          }]
      },
      {
        name: "Travel",
        id: 8,
        children: [{
            name: "Accessories",
            id: 53,
          },{
            name: "Activities",
            id: 54,
          },{
            name: "Lodging",
            id: 55,
          },{
            name: "Planes, Trains and Automobiles",
            id: 56,
          }]
      },
      {
        name: "Electronics & Office",
        id: 9,
        children: [{
            name: "Computers & Parts",
            id: 57,
          },{
            name: "Home Entertainment",
            id: 58,
          },{
            name: "Office Supplies",
            id: 59,
          },{
            name: "Smartphone & Tablets",
            id: 60,
          }]
      },
      {
        name: "Auto",
        id: 10,
        children: [{
            name: "Electronics & Accessories",
            id: 61,
          },{
            name: "Parts",
            id: 62,
          },{
            name: "Maintenance",
            id: 63,
          },{
            name: "Tires & Wheels",
            id: 64,
          }]
      },
      {
        name: "Baby & Toddler",
        id: 11,
        children: [{
            name: "Baby Food",
            id: 65,
          },{
            name: "Baby Necessities",
            id: 66,
          },{
            name: "Bedding & Furnishings",
            id: 67,
          },{
            name: "Clothing & Toys",
            id: 68,
          }]
      },
      {
        name: "Services",
        id: 12,
        children: [{
            name: "Auto Services",
            id: 69,
          },{
            name: "Home Services",
            id: 70,
          },{
            name: "Personal Services",
            id: 71,
          },{
            name: "Professional Services",
            id: 72,
          }]
      },
      {
        name: "Toys",
        id: 13,
        children: [{
            name: "Boy's Toys",
            id: 73,
          },{
            name: "Electronics",
            id: 74,
          },{
            name: "Games",
            id: 75,
          },{
            name: "Girl's Toys",
            id: 76,
          }]
      },
      {
        name: "Books/Music/Media",
        id: 14,
        children: [{
            name: "Digital Mediums",
            id: 77,
          },{
            name: "DVD's, CD's & Vinyl",
            id: 78,
          },{
            name: "Hard/Paperback Books & Magazines",
            id: 79,
          },{
            name: "Software/Games",
            id: 80,
          }]
      },
      {
        name: "Flowers & Gifts",
        id: 15,
        children: [{
            name: "Flowers",
            id: 81,
          },{
            name: "Gifts",
            id: 82,
          },{
            name: "Plants",
            id: 83,
          },{
            name: "Sweets",
            id: 84,
          }]
      },
      {
        name: "Jewelry & Accessories",
        id: 16,
        children: [{
            name: "Handbags",
            id: 85,
          },{
            name: "Hats & Glasses",
            id: 86,
          },{
            name: "Watches and Jewelry",
            id: 87,
          },{
            name: "Wearables",
            id: 88,
          }]
      },

  ];

export default class Interest extends Component {
    constructor() {
        super();
        this.state = {
            selected: new Map(),
            loading: false,
            myInterests:[]
        }
        firebase.analytics().setCurrentScreen('Interest');
    }

     componentDidMount() {
      InteractionManager.runAfterInteractions(() => {

       this.getInterestList();
      });
        // const selectedState = new Map();
        // interestItem.map((category) => {
        //     selectedState.set(category.name, category.children)
        // });
        // this.setState({selected: selectedState});
        // interestItem.map((category) => {
        //     this.subCategoryHandler(category.name,category.children,false)
        // });
    }

getInterestList = async() => {

  var _this = this
  this.setState({ loading: true });
  try {
      let response = await api.getRequestApi('interests');
      console.log('--------------- get interests List response-----------')
      console.log(response)
      setTimeout(function(){
        _this.setState({ loading: false });
      }, 2000);
     
      if (response.status === 200) {
          try {
              await response.json().then((data) => {
                  _this.setState({myInterests:data.interest});
                  _this.showInterestList();
              })
          } catch (error) {
              console.log(error)
          }
      }
  } catch (error) {
      console.log(error)
  }
}

showInterestList = () => {
  const selectedState = new Map();
  interestItem.map((category) => {
      selectedState.set(category.name, category.children)
  });
  this.setState({selected: selectedState});
  interestItem.map((category) => {
     this.subCategoryHandler(category.name,category.children,true)
      var subCat = category.children;
 
      subCat.map((subObj) => {
         if(this.findArrayElementById(subObj.id)){
            this.subCategoryHandler(category.name,subObj)
          }
        })
  });
}

   findArrayElementById (id) {
    for (let i = 0; i < this.state.myInterests.length; i++) {
      if (id === this.state.myInterests[i]){
          return true
       }
      }
    return false
  }

    categoryHandler = (item) => {
        this.setState((state) => {
            const selected = new Map(state.selected);
            const currentSelection = selected.get(item);
            if (currentSelection) {
                selected.set(item, null);
            } else {
                selected.set(item, []);
            }
            return {selected};
        })
    };

    subCategoryHandler = (item, sub, type) => {
       
        if (type) {
            this.setState((state) => {
                const selected = new Map(state.selected);
                selected.set(item, []);
                return {selected};
            })
        } else {
            this.setState((state) => {
                const selected = new Map(state.selected);
                let getSelected = selected.get(item);
                const subIndex = getSelected.indexOf(sub);
                if (subIndex >= 0) {
                    getSelected.splice(subIndex, 1);
                } else {
                    getSelected = getSelected.concat(sub);
                    getSelected = getSelected.filter(function (item, pos) {
                        return getSelected.indexOf(item) == pos
                    });
                }
                selected.set(item, getSelected);
                return {selected};
            });
        }
    };

    submitBtnAction = () =>{
        this.setState({ loading: true });
        var selectedArray=[];
        const selected = new Map(this.state.selected);
        interestItem.map((item) => {
            let getSelected = selected.get(item.name);
            if (getSelected){
            getSelected.map((sub) =>{
            selectedArray.push(sub.id)
            })}
        })
        this.updateOnServer(selectedArray)
    }

    updateOnServer = async (selectedArray) =>{
        let body = {
            "categories": selectedArray
                };
        try {
            let response = await api.request('add-interests', 'post', body);
            this.setState({ loading: false });
            if (response.status === 200) {
               // navigate.navigateWithReset(this.props.navigation, 'Dashboard')
               navigate.navigateWithReset(this.props.screenProps.rootNavigation, 'SignedIn')

            } else {
               
            }
        } catch (error) {
            this.setState({ loading: false });
            console.log(error);
            alert("There was an error processing your data. Please try again.")
        }s
    }

    skipMethod(){
      navigate.navigateWithReset(this.props.screenProps.rootNavigation, 'SignedIn')
    }

    render() {
        const {categories, selected} = this.state;
        return (
            <Background
                onPressSkip={this.skipMethod.bind(this)}
                skip={'Skip'}
              >
                <SilverBox paddingBottom={40}>
                    <SilverBoxHeader interest/>
                    <FlatList
                        ref={'list'}
                        // data={categories}
                        data={interestItem}
                        extraData={this.state.selected}
                        keyExtractor={(item, index) => item.name}
                        renderItem={({item, index}) =>
                            <View>
                                <View>
                                    <TextBox checkboxList item={item.name} sub={item.children} selected={selected.get(item.name)}
                                             categoryHandler={this.categoryHandler} select={this.subCategoryHandler}/>
                                </View>
                            </View>
                        }
                    />
                </SilverBox>
                <View style={{marginVertical: 20}}>
                    <Button name={'DONE'} paddingHorizontal={18}
                            //  onPress={() => navigate.navigateWithReset(this.props.navigation, 'Dashboard')}    
                            onPress={() => this.submitBtnAction()}                        />
                </View>
                {this.state.loading ?
                <Loader/> : null
                }
            </Background>
        )
    }

}