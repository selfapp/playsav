import React, {Component} from 'react';
import { 
    Animated,
    InteractionManager
} from 'react-native';
import firebase from 'react-native-firebase';
import Background from '../Components/background';
import GameTypeBox from '../Components/game_type_box';
import navigate from '../Components/navigate';
import api from '../api';
import Loader from '../Components/activityIndicator'

export default class GameTypes extends Component {
    constructor() {
        super();
        this.state = {
            opacity: new Animated.Value(0),
            defaultGameTypes:[],
            loading: false
        }
        firebase.analytics().setCurrentScreen('Game Type');
    }

    componentDidMount() {

        InteractionManager.runAfterInteractions(() => {
            this.getGameTypeList()
            Animated.timing(
                this.state.opacity, {toValue: 1, duration: 1500}
            ).start();
        });
    }

    getGameTypeList = async()=>{
    var _this = this
    this.setState({loading:true})
        try {
            let response = await api.getRequestApi('cats');
            console.log('--------------- get game type list-----------')
            console.log(response)
            if (response.status === 200) {
                 _this.setState({ loading: false });
                try {
                    await response.json().then((data) => {
                        _this.setState({defaultGameTypes:data.categories})
                        console.log(_this.state.defaultGameTypes)
                    })
                } catch (error) {
                    console.log(error)
                }
            }
        } catch (error) {
            _this.setState({ loading: false });
            console.log(error)
        }
    }



    getSelectedGameType = (selected_game_type, count) => {
        navigate.navigateTo(this.props.screenProps.SignedInStackNavigator, 'Games',
        {gameKey: selected_game_type,  subCategoryCount: count, fromCoupon:false})
       
    };

    render() {
        return (
            <Background
            useBackGroundImage
            home={require('../Assets/home.png')} leftToDashboard={require('../Assets/back.png')}
            navigation={this.props.screenProps.SignedOutNavigation}>
            {this.state.loading ?
                <Loader/> : null
                }
            {/* navigation={this.props.navigation}> */}
            
            {/* // navigation={this.props.screenProps.SignedOutNavigation}> */}

                {/* <Animated.View useNativeDriver style={{
                    marginTop: 5,
                    opacity: this.state.opacity,
                 }}> */}
                    <GameTypeBox gameTypes={this.state.defaultGameTypes}
                                 getSelectedGameType={this.getSelectedGameType}
                    />
                {/* </Animated.View> */}
                
                
            </Background>
        )
    }
}