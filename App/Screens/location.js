import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import MapView, {Marker, Circle} from 'react-native-maps';
import SoundPlayer from 'react-native-sound-player';
import firebase from 'react-native-firebase';

import api from '../api';

export default class Location extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude: 47.609243,
            longitude: -122.325177,
            nearStore: []
        };
        firebase.analytics().setCurrentScreen('Location');
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                SoundPlayer.pause()

                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
                this.nearByStores()
            },
            (error) => this.setState({error: error.message}),
            {enableHighAccuracy: true, timeout: 2000, maximumAge: 2000},
        );
    }

    nearByStores = async () => {
        let body = {
            "lat": this.state.latitude,
            "lng": this.state.longitude,
        };

        try {
            let response = await api.request('near-by-stores', 'post', body);

            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        this.setState({nearStore: data.stores});
                    });
                } catch (error) {
                    this.setState({loading: false});
                    alert("There was an error processing your data. Please try again.")
                }
            } else {
                await response.json().then((res) => {
                    this.setState({loading: false, error: res.error});
                });
            }
        } catch (error) {
            this.setState({loading: false});
            alert("There was an error processing your data. Please try again.")
        }
    };


    render() {
        let {latitude, longitude} = this.state;
        return (
            <MapView
                style={{flex: 1}}
                region={{
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.0722,
                    longitudeDelta: 0.0221,
                }}
            >
                <Circle center={{latitude: this.state.latitude, longitude: this.state.longitude}} radius={3000}
                        fillColor={'rgba(65, 145, 170, 0.26)'} strokeColor={'transparent'}/>
                <Marker coordinate={{latitude: this.state.latitude, longitude: this.state.longitude}}
                        image={require('../Assets/map-icon.png')}
                />

                {this.state.nearStore.map((item, key) => {
                    return (
                        <Marker coordinate={{latitude: item.lat, longitude: item.lng}}
                                title={item.company_name}
                                description={item.address}
                        >
                            <View style={styles.marker}/>
                        </Marker>
                    )

                })}
            </MapView>
        )
    }
}

const styles = StyleSheet.create({
    marker: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: 'white',
        borderWidth: 5,
        borderColor: '#4191AA'
    }
});
