import React, { Component } from 'react';
import { FlatList,
     View,
     Image,
     Text,
     StyleSheet,
     TouchableOpacity,
     Dimensions,
     ActivityIndicator
 } from 'react-native';
import Background from '../Components/background';
import navigate from '../Components/navigate';
import GradientSilverBox from '../Components/gradient_silver_box';
import api from '../api';
import Swiper from 'react-native-swiper';
import firebase from 'react-native-firebase';

const width = Dimensions.get("window").width;

export default class Spotlight extends Component {
    constructor() {
        super();
        this.onEndReachedCalledDuringMomentum = true;
        this.state = {
            imgList: [
                'https://avatars3.githubusercontent.com/u/533360?v=3&s=466',
                'https://assets-cdn.github.com/images/modules/site/business-hero.jpg',
                'https://placeholdit.imgix.net/~text?txtsize=29&txt=350%C3%971150&w=350&h=1150'
            ],
            loading: false,
            couponsSwiperData: [],
            topSwiper: [],
            bottomFlatList: [],
            pageNo:1,
            allCoupon:[],
            totalPage: 1,
            enableScrollViewScroll: true
        };
        firebase.analytics().setCurrentScreen('spotlight');

    }

    componentDidMount() {
        this.getCouponList()
    }

    getCouponList = async () => {
        var _this = this;
        this.setState({ loading: true });
        try {
            let response = await api.getRequestApi('all-coupons/1');
            this.setState({ loading: false });
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        let allCouponData = data.coupons.slice()
                        let topData = data.coupons.splice(0, 5);
                        let bottomData = data.coupons;
                        _this.setState({
                            allCoupon: allCouponData,
                            topSwiper: topData,
                            bottomFlatList: bottomData,
                            totalPage: data.pages
                        });
                    })
                } catch (error) {
                    console.log(error)
                }
            }
        } catch (error) {
            this.setState({ loading: false });
            console.log(error)
        }
    };

    fetchCouponList = async () => {
        var _this = this;
        this.setState({ loading: true });
        try {
            let response = await api.getRequestApi('all-coupons/' + this.state.pageNo);
            this.setState({ loading: false });
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        _this.setState({
                            allCoupon: this.state.allCoupon.concat(data.coupons),
                            bottomFlatList: this.state.bottomFlatList.concat(data.coupons)
                        });
                    })
                } catch (error) {
                    console.log(error)
                }
            }
        } catch (error) {
            this.setState({ loading: false });
            console.log(error)
        }
    };

    onEndReached(){
        if(!this.onEndReachedCalledDuringMomentum){
            console.log('called')
            this.onEndReachedCalledDuringMomentum = true;
            this.setState({ pageNo: this.state.pageNo + 1})
            if(this.state.pageNo > this.state.totalPage)
            {
                return;
            }
            this.fetchCouponList()
        }
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => navigate.navigateTo(this.props.navigation, 'SpotlightCouponDetail', { item: item })}>
                <View style={{
                    marginHorizontal: 15,
                    marginTop: 5,
                    borderWidth: 2,
                    borderColor: '#525252',
                    borderStyle: 'dashed',
                    borderRadius: 5
                }}>
                    <GradientSilverBox>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ width: '25%', alignItems: 'flex-start', justifyContent: 'center' }}>
                                <Image style={{ width: 55, height: 55, marginLeft: 5, marginTop: 2 }}
                                       source={{ uri: item.logo, cache: 'force-cache' }}
                                       resizeMode="contain" />
                            </View>
                            {/* <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                                <Image source={require('../Assets/mcdonalds-logo.png')}/>
                            </View> */}
                            <View style={{ width: '65%' }}>
                                <Text style={{ color: '#000000', fontSize: 18, fontWeight: 'bold' }}>{item.title}</Text>
                                <Text numberOfLines={1}>{item.description}</Text>
                            </View>
                            <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../Assets/cpn-arrow.png')} />
                            </View>
                        </View>
                    </GradientSilverBox>
                </View>
            </TouchableOpacity>
        )
    };

    renderFooter = () => {
        if (!this.state.loading) return null;
        return (
          <View
            style={{
              paddingVertical: 20,
            }}
          >
            <ActivityIndicator animating size="large" color='rgb(254,254,254)' />
          </View>
        );
      };

headerItem = ({ item }) =>{

    return(
            <View>
                <View style={{ width: width, height: 200, alignItems: 'center', justifyContent: 'center' }}>
                    <Swiper
                        horizontal={true}
                        autoplay ={true}
                        height={150}
                        width={Dimensions.get('window').width}
                        key={Date.now()}
                        autoplayTimeout= {5}
                        showsPagination={true}
                        loop={true}
                        dot={<View style={{backgroundColor:'#4191AA', width: 16, height: 16,borderRadius: 8, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
                        activeDot = {<View style={{backgroundColor: 'white', width: 16, height: 16, borderRadius: 8, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
                        buttonWrapperStyle = {{backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: -20, left: 0, flex: 1, paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'space-between', alignItems: 'center'}	}
                        nextButton = {<View style={[styles.triangle, {transform: [{rotate: '90deg'}]}]}/>}
                        prevButton = {<View style={[styles.triangle, {transform: [{rotate: '-90deg'}]}]}/>}
                        showsButtons
                    >
                        {this.state.topSwiper.map((item, key) => {
                            return (
                                <View key={key} style={{alignItems: 'center'}}>
                                    {console.log(item)}
                                    <TouchableOpacity onPress={() => navigate.navigateTo(this.props.navigation, 'SpotlightCouponDetail', { item: item })}>

                                    <GradientSilverBox height={150}>
                                        <Image
                                            style={{ width: 230, height: 150}}
                                            source={{ uri: item.image,cache: 'force-cache' }}
                                            resizeMode="contain"
                                        />
                                    </GradientSilverBox>
                                    </TouchableOpacity>
                                </View>
                            )
                        })}
                    </Swiper>
                </View>
                <View style={{height: 120, width: '100%'}}>
                <FlatList
                    horizontal={true}
                    style={{
                        borderWidth: 0, borderColor: '#CF7C39', borderRadius: 5,
                        marginBottom: 20, height: 100, width: '100%'
                    }}
                    data={this.state.bottomFlatList}
                    renderItem={({ item }) =>
                    <TouchableOpacity onPress={() => navigate.navigateTo(this.props.navigation, 'SpotlightCouponDetail', { item: item })}>
                        <View style={{ width: Dimensions.get('window').width / 3,height:100, alignItems: 'center', justifyContent: 'center' }}>
                            <GradientSilverBox height={100}>
                                <Image
                                    style={{ width: Dimensions.get('window').width / 3.4, height: 100 }}
                                    source={{ uri: item.image, cache: 'force-cache' }}
                                    resizeMode='contain'
                                />
                            </GradientSilverBox>
                        </View>
                        </TouchableOpacity>
                    }
                />
                </View>
        </View>

    )
}


    render() {
        return (
            <Background withoutSroll={true} home={require('../Assets/home.png')} leftToDashboard={require('../Assets/back.png')}
                        navigation={this.props.screenProps.SignedOutNavigation}>
   
                <FlatList
                ListFooterComponent={this.renderFooter}
                    ListHeaderComponent={this.headerItem}
                    data={this.state.allCoupon}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this.renderItem}
                    onEndReached={ () => this.onEndReached()}
                    onEndReachedThreshold={0.5}
                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                />
            </Background>
        )
    }
}

const styles = StyleSheet.create({
    triangle: {
        borderLeftWidth: 18,
        borderRightWidth: 18,
        borderBottomWidth: 25,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'white',
        opacity: 0.29
    }
});