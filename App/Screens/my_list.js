import React, {Component} from 'react';
import {
    Text,
    FlatList,
    TouchableOpacity,
    Image,
    View,
    TouchableWithoutFeedback,
    Modal,
    Dimensions,
    KeyboardAvoidingView,
    TextInput,
    PixelRatio,
    ActivityIndicator
} from 'react-native';
import Background from '../Components/background';
import GradientSilverBox from '../Components/gradient_silver_box';
import navigate from '../Components/navigate';
import api from '../api';
import Loader from '../Components/activityIndicator'

export default class MyList extends Component {
    constructor() {
        super();
        this.state = {
            modalVisible: false,
            scrollPosition: 0,
            myList: [],
            loading: false,
            isRename: false,
            listName: ''
        }
    }

    componentDidMount() {
        this.getList()
    }

    getList = async () => {
        var _this = this;
        this.setState({loading: true});
        try {
            let response = await api.getRequestApi('my-list');
            console.log('--------------- get List response-----------');
            console.log(response);
            this.setState({loading: false});
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        _this.setState({myList: data.list})
                    })
                } catch (error) {
                    console.log(error)
                }
            }
        } catch (error) {
            console.log(error)
        }
    };
    removeList = async (index) => {
        var _this = this;
        this.setState({loading: true});
        var item = this.state.myList[index];
        console.log('----', item);
        try {
            let response = await api.delRequestApi("list/" + item.id);
            console.log('--------------- Remove Coupon from List response-----------');
            console.log(response);
            this.setState({loading: false});
            if (response.status === 200) {
                _this.getList()
            }
        } catch (error) {
            this.setState({loading: false});
            console.log(error)
        }
    };

    renameList = async (index) => {
        this.setState({modalVisible: false});
        this.setState({loading: true});
        let body = {
            "name": this.state.listName,
        };
        this.setState({listName: ''});
        var url = 'list/' + this.state.myList[index].id;
        try {
            let response = await api.request(url, 'put', body);
            this.setState({loading: false});
            if (response.status === 200) {
                console.log(response);
                this.getList()
            }
        } catch (e) {
            this.setState({loading: false});
            console.log(e)
        }

    }


    renderItem = ({item, index}) => {
        return (
            <View>
                <TouchableOpacity style={{marginTop: 15}}
                                  onPress={() => navigate.navigateTo(this.props.navigation, 'MyListCoupons', {
                                      item: item,
                                      delegate: this
                                  })}>
                    <GradientSilverBox height={80}>
                        <View style={{width: '100%', justifyContent: 'center', paddingLeft: 20}}>
                            <Text style={{fontWeight: 'bold', fontSize: 20}}>{item.name}</Text>
                            <Text style={{fontSize: 16, marginTop: 10}}>{item.text}</Text>
                            <TouchableWithoutFeedback onPress={() => this.setState({modalVisible: true, index})}>
                                <View style={{
                                    position: 'absolute',
                                    right: 0,
                                    width: 40,
                                    alignItems: 'center',
                                    height: 50,
                                    justifyContent: 'center'
                                }}>
                                    <Image source={require('../Assets/option.png')}/>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </GradientSilverBox>
                </TouchableOpacity>
            </View>
        )
    };


    modal() {
        if (this.state.isRename) {
            return (
                <Modal animationType="fade" transparent={true} visible={this.state.modalVisible ? true : false}>
                    <TouchableWithoutFeedback onPress={() => {
                        this.setState({modalVisible: false, isRename: false})
                    }}>
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'rgba(0,0,0,0.67)'
                        }}>
                            <KeyboardAvoidingView behavior="position" enabled
                                                  style={{backgroundColor: '#E5E5E5', width: '80%'}}>
                                <Text style={{alignSelf: 'center', fontSize: 20, fontWeight: 'bold'}}>Rename</Text>
                                <View style={{flexDirection: 'row', justifyContent: 'center', marginVertical: 20}}>
                                    <TextInput placeholder={'Enter new list name'} placeholderTextColor={'#CBCBCB'}
                                               autoCorrect={false}
                                               onChangeText={(listName) => this.setState({listName})}
                                               value={this.state.listName}
                                               underlineColorAndroid={'transparent'}
                                               style={{
                                                   backgroundColor: '#8E8C8C',
                                                   height: 50,
                                                   width: '80%',
                                                   paddingLeft: 10,
                                                   color: 'white'
                                               }}/>
                                </View>
                                <View>
                                    <Button name={'Rename'} paddingHorizontal={10} fontSize={PixelRatio.get() * 8}
                                            onPress={() => {
                                                this.renameList(this.state.index);
                                                this.setState({modalVisible: false, isRename: false})
                                            }}/>
                                </View>
                            </KeyboardAvoidingView>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            )

        } else {
            return (
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                    <TouchableWithoutFeedback onPress={() => this.setState({modalVisible: false})}>
                        <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.1)'}}>
                            <View style={{
                                backgroundColor: '#909090',
                                borderRadius: 7,
                                width: 120,
                                padding: 10,
                                alignSelf: 'flex-end',
                                top: ((Dimensions.get('window').width * 0.6) + (this.state.index * 95) - (this.state.scrollPosition)),
                                right: '12%'
                            }}>

                                <TouchableOpacity onPress={() => {
                                    this.removeList(this.state.index);
                                    this.setState({modalVisible: false})
                                }}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image source={require('../Assets/trash.png')}/>
                                        <Text style={{marginLeft: 10}}>Delete List</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => {
                                    this.setState({modalVisible: false, isRename: true});
                                    this.setState({modalVisible: true})
                                }}>
                                    <View style={{flexDirection: 'row', marginTop: 10}}>
                                        <Image source={require('../Assets/rename.png')}/>
                                        <Text style={{marginLeft: 10}}>Rename List</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            )
        }
    }

    handleScroll = (event) => {
        this.setState({scrollPosition: event.nativeEvent.contentOffset.y})
    };
    renderFooter = () => {
        if (!this.state.loading) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                }}
            >
                <ActivityIndicator animating size="large" color='rgb(254,254,254)'/>
            </View>
        );
    };

    render() {
        return (
            <Background home={require('../Assets/home.png')} leftToDashboard={require('../Assets/back.png')}
                        navigation={this.props.screenProps.SignedOutNavigation} handleScroll={this.handleScroll}>
                <Text style={{color: 'white', fontSize: 18}}>LIST</Text>
                <View style={{width: '90%'}}>
                    <FlatList
                        ListFooterComponent={this.renderFooter}
                        //data={[{name: 'Birthday', count: 4}, {name: 'New Year', count: 2}, {name: 'Easter', count: 6}, {name: 'Christmas', count: 3}]}
                        data={this.state.myList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this.renderItem}
                    />
                </View>

                {this.modal()}
                {/* {this.state.loading ?
                <Loader/> : null
                } */}
            </Background>
        )
    }
}
