import React, { Component } from "react";
import {
    Text,
    Image,
    View,
    FlatList,
    Animated,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import Background from "../Components/background";
import GradientSilverBox from "../Components/gradient_silver_box";
import navigate from "../Components/navigate";
import api from "../api";
import SoundPlayer from "react-native-sound-player";
import FastImage from "react-native-fast-image";
import SegmentedControlTab from "react-native-segmented-control-tab";
import firebase from "react-native-firebase";

export default class Coupons extends Component {
    _isMounted = false;

    constructor() {
        super();
        this.onEndReachedCalledDuringMomentum = true;
        this.state = {
            opacity: new Animated.Value(0),
            loading: false,
            customStyleIndex: 0,
            selectedIndex: 0,
            newCoupns: [],
            expireCoupns: [],
            allCoupns: [],
            newPageNo: 2,
            expirePageNo: 2,
            allPageNo: 2,
            newNoPages: 1,
            expireNoPages: 1,
            allNoPages: 1,
            isFetching: false
        };
        firebase.analytics().setCurrentScreen("Coupons");
    }

    componentDidMount() {
        this._isMounted = true;
        this.getCouponList("new");
        this.getCouponList("expiring");
        this.getCouponList("all");

        Animated.timing(this.state.opacity, { toValue: 1, duration: 1500 }).start();
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    getCouponList = async typeName => {
        SoundPlayer.pause();
        var _this = this;

        this.setState({ loading: true });
        try {
            let response = await api.getRequestApi(
                "my-coupon-listing/" + typeName + "/1"
            );
            if (_this._isMounted) {
                this.setState({ loading: false });
            }
            console.log(response);
            if (response.status === 200) {
                try {
                    await response.json().then(data => {
                        console.log(JSON.stringify(data));
                        if (_this._isMounted) {
                            if (typeName === "new") {
                                _this.setState({
                                    newCoupns: data.coupons.data,
                                    newNoPages: data.coupons.pages
                                });
                            }
                            if (typeName === "expiring") {
                                _this.setState({
                                    expireCoupns: data.coupons.data,
                                    expireNoPages: data.coupons.pages
                                });
                            }
                            if (typeName === "all") {
                                _this.setState({
                                    allCoupns: data.coupons.data,
                                    allNoPages: data.coupons.pages
                                });
                            }
                        }
                    });
                } catch (error) {
                    console.log(error);
                }
            }
        } catch (error) {
            this.setState({ loading: false });
            console.log(error);
        }
    };

    fetchCouponList = async typeName => {
        SoundPlayer.pause();
        var _this = this;
        var pageno = 1;
        switch (typeName) {
            case "new": {
                pageno = this.state.newPageNo;
                if (pageno > this.state.newNoPages) {
                    return;
                }
                this.setState({ newPageNo: this.state.newPageNo + 1 });
                break;
            }
            case "expiring": {
                pageno = this.state.expirePageNo;
                if (pageno > this.state.expireNoPages) {
                    return;
                }
                this.setState({ expirePageNo: this.state.expirePageNo + 1 });
                break;
            }
            case "all": {
                pageno = this.state.allPageNo;
                console.log(pageno, this.state.allNoPages);
                if (pageno > this.state.allNoPages) {
                    return;
                }
                this.setState({ allPageNo: this.state.allPageNo + 1 });
                break;
            }
            default:
                pageno = 1;
                break;
        }
        this.setState({ loading: true });
        try {
            let response = await api.getRequestApi(
                "my-coupon-listing/" + typeName + "/" + pageno
            );
            if (_this._isMounted) {
                this.setState({ loading: false });
            }
            if (response.status === 200) {
                try {
                    await response.json().then(data => {
                        if (_this._isMounted) {
                            if (typeName === "new") {
                                _this.setState({
                                    newCoupns: _this.state.newCoupns.concat(data.coupons.data),
                                    newNoPages: data.coupons.pages
                                });
                            }
                            if (typeName === "expiring") {
                                _this.setState({
                                    expireCoupns: _this.state.expireCoupns.concat(
                                        data.coupons.data
                                    ),
                                    expireNoPages: data.coupons.pages
                                });
                            }
                            if (typeName === "all") {
                                _this.setState({
                                    allCoupns: _this.state.allCoupns.concat(data.coupons.data),
                                    allNoPages: data.coupons.pages
                                });
                            }
                        }
                    });
                } catch (error) {
                    console.log(error);
                }
            }
        } catch (error) {
            this.setState({ loading: false });
            console.log(error);
        }
    };

    onRefresh() {
        this.setState({ isFetching: true }, () => {
            this.getCouponList("new");
        });
        this.setState({ isFetching: false });
    }
    onEndReached(typeName) {
        if (!this.onEndReachedCalledDuringMomentum) {
            console.log("called");
            this.onEndReachedCalledDuringMomentum = true;
            this.fetchCouponList(typeName);
        }
    }

    handleCustomIndexSelect = index => {
        this.setState({
            ...this.state,
            customStyleIndex: index
        });
    };
    renderFooter = () => {
        if (!this.state.loading) return null;
        return (
            <View
                style={{
                    paddingVertical: 20
                }}
            >
                <ActivityIndicator animating size="large" color="rgb(254,254,254)" />
            </View>
        );
    };

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() =>
                    navigate.navigateTo(this.props.navigation, "CouponDetail", {
                        item: item,
                        delegate: this
                    })
                }
            >
                <Animated.View
                    useNativeDriver
                    style={{
                        marginHorizontal: 15,
                        marginTop: 5,
                        borderWidth: 2,
                        borderColor: "#525252",
                        borderStyle: "dotted",
                        borderRadius: 5,
                        opacity: this.state.opacity
                    }}
                >
                    <GradientSilverBox>
                        <View style={{ flexDirection: "row" }}>
                            <View
                                style={{
                                    width: "25%",
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}
                            >
                                {item.logo ? (
                                    <FastImage
                                        style={{
                                            width: 55,
                                            height: 55,
                                            marginLeft: -10,
                                            marginTop: 2
                                        }}
                                        source={{
                                            uri: item.logo,
                                            headers: { Authorization: "someAuthToken" },
                                            priority: FastImage.priority.normal
                                        }}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                ) : (
                                    <Image source={require("../Assets/cpn-arrow.png")} />
                                )}
                            </View>
                            <View style={{ width: "65%" }}>
                                <Text
                                    style={{ color: "#000000", fontSize: 18, fontWeight: "bold" }}
                                    numberOfLines={1}
                                >
                                    {item.offer}
                                </Text>
                                <Text numberOfLines={1}>{item.description}</Text>
                            </View>
                            <View
                                style={{
                                    width: "10%",
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}
                            >
                                <Image source={require("../Assets/cpn-arrow.png")} />
                            </View>
                        </View>
                    </GradientSilverBox>
                </Animated.View>
            </TouchableOpacity>
        );
    };

    render() {
        return (
            <Background
                withoutSroll={true}
                home={require("../Assets/home.png")}
                leftToDashboard={require("../Assets/back.png")}
                navigation={this.props.screenProps.SignedOutNavigation}
            >
                <SegmentedControlTab
                    allowFontScaling={false}
                    values={["NEW", "EXPIRING", "ALL"]}
                    selectedIndex={this.state.customStyleIndex}
                    onTabPress={this.handleCustomIndexSelect}
                    borderRadius={0}
                    tabsContainerStyle={{
                        height: 50,
                        backgroundColor: "#ffffff",
                        borderWidth: 0.5,
                        borderColor: "#ffffff"
                    }}
                    tabStyle={{
                        backgroundColor: "#ffffff",
                        borderWidth: 0,
                        borderColor: "transparent"
                    }}
                    activeTabStyle={{
                        backgroundColor: "#ffffff",
                        marginTop: 0,
                        borderWidth: 1,
                        borderColor: "#ffffff",
                        borderBottomWidth: 2,
                        borderBottomColor: "#585858"
                    }}
                    tabTextStyle={{
                        color: "#888888",
                        fontSize: 20
                    }}
                    activeTabTextStyle={{ color: "#888888" }}
                />

                {this.state.customStyleIndex === 0 && (
                    <View>
                        <FlatList
                            ListFooterComponent={this.renderFooter}
                            style={{ marginBottom: 50 }}
                            data={this.state.newCoupns}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={this.renderItem}
                            onRefresh={() => this.onRefresh()}
                            refreshing={this.state.isFetching}
                            onEndReached={() => this.onEndReached("new")}
                            onEndReachedThreshold={0.5}
                            onMomentumScrollBegin={() => {
                                this.onEndReachedCalledDuringMomentum = false;
                            }}
                            ListEmptyComponent={() => {
                                return <View
                                    style={{
                                        alignItems: "center",
                                        justifyContent: "center",
                                        marginTop: 100
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: "#ffffff",
                                            justifyContent: "center",
                                            fontSize: 20
                                        }}
                                    >
                                        {" "}
                                        No Items Found
                                    </Text>
                                </View>
                            }}
                        />
                    </View>
                )}
                {this.state.customStyleIndex === 1 && (
                    <View>
                        {this.state.expireCoupns.length ? (
                            <FlatList
                                ListFooterComponent={this.renderFooter}
                                style={{ marginBottom: 50 }}
                                removeClippedSubviews={true}
                                data={this.state.expireCoupns}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={this.renderItem}
                                onRefresh={() => this.onRefresh()}
                                refreshing={this.state.isFetching}
                                onEndReached={() => this.onEndReached("expiring")}
                                onEndReachedThreshold={0.5}
                                onMomentumScrollBegin={() => {
                                    this.onEndReachedCalledDuringMomentum = false;
                                }}
                            />
                        ) : (
                            <View
                                style={{
                                    alignItems: "center",
                                    justifyContent: "center",
                                    marginTop: "30%"
                                }}
                            >
                                <Text
                                    style={{
                                        color: "#ffffff",
                                        justifyContent: "center",
                                        fontSize: 20
                                    }}
                                >
                                    {" "}
                                    No Items Found
                                </Text>
                            </View>
                        )}
                    </View>
                )}
                {this.state.customStyleIndex === 2 && (
                    <View>
                        {this.state.allCoupns.length ? (
                            <FlatList
                                ListFooterComponent={this.renderFooter}
                                style={{ marginBottom: 50 }}
                                removeClippedSubviews={true}
                                data={this.state.allCoupns}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={this.renderItem}
                                onRefresh={() => this.onRefresh()}
                                refreshing={this.state.isFetching}
                                onEndReached={() => this.onEndReached("all")}
                                onEndReachedThreshold={0.5}
                                onMomentumScrollBegin={() => {
                                    this.onEndReachedCalledDuringMomentum = false;
                                }}
                            />
                        ) : (
                            <View
                                style={{
                                    alignItems: "center",
                                    justifyContent: "center",
                                    marginTop: "30%"
                                }}
                            >
                                <Text
                                    style={{
                                        color: "#ffffff",
                                        justifyContent: "center",
                                        fontSize: 20
                                    }}
                                >
                                    {" "}
                                    No Items Found
                                </Text>
                            </View>
                        )}
                    </View>
                )}

                {/* {this.state.loading ?
                <Loader/> : null
                } */}
            </Background>
        );
    }
}
