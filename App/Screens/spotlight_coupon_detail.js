import React, {Component} from 'react';
import {Image, Text, View, PixelRatio,WebView,Alert,ImageBackground,Dimensions} from 'react-native';
import Background from '../Components/background';
import SilverBox from '../Components/silver_box';
import Button from '../Components/button';
import navigate from '../Components/navigate';
import api from '../api';
//import Barcode from 'react-native-barcode-builder';
import ViewMoreText from 'react-native-view-more-text';


export default class SpotlightCouponDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            couponMedia:[],
            totalCount: 0,
            isMoreView:true
        }
       
    }

componentDidMount(){
  this.getCouponMedia()
}

getCouponMedia = async () =>{
    var _this = this
console.log(this.props.navigation.state.params.item)
    try {
        let response = await api.getRequestApi('coupon-related-media/' + this.props.navigation.state.params.item.id);
        console.log('--------------- get Coupon media response-----------')
        console.log(response)
        if (response.status === 200) {
            try {
                await response.json().then((data) => {
                    console.log(JSON.stringify(data))
                    _this.setState({couponMedia:data.categories[0].ids, totalCount: data.categories[0].count})
                })
            } catch (error) {
                console.log(error)
            }
        }
    } catch (error) {
        console.log(error)
    }
}

playGame = () =>{
    console.log('#################')
    console.log('from coupon = '+this.state.couponMedia+'  total count = '+this.state.totalCount)
    navigate.navigateTo(this.props.screenProps.SignedOutNavigation, 'Games',{gameKey: this.state.couponMedia, subCategoryCount: this.state.totalCount, fromCoupon:true})
}
renderViewMore(onPress){
    return(
      <Text style={{color:'#3E86A3',marginLeft:10}} onPress={onPress}>View more</Text>
    )
  }
  renderViewLess(onPress){
    return(
      <Text style={{color:'#3E86A3',marginLeft:10}} onPress={onPress}>View less</Text>
    )
  }
    render() {
        const iphoneHeight = Dimensions.get('window').height
         //var str = 'jhgjbvjbbbvjhbdfbvduuujfjvbadgjlhlgldhglidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivhjhgjbvjbbbvjhbdfbvduuujfjvbadgjlhlgldhglidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivhjhgjbvjbbbvjhbdfbvduuujfjvbadgjjgjkgjkghvjhlidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivh'
         var countstr = this.props.navigation.state.params.item.length
         var temp
         if (!this.state.isMoreView){
         if (countstr > 400){
           temp = 700
         }else if(countstr > 300){
            temp = 650
         }else if(countstr > 200){
          temp = 600
         }else if(countstr > 100){
         temp = 550
         }else{
             temp = 500
         }
     }
     var viewHeight = (this.state.isMoreView) ? (iphoneHeight * 58) / 100 : temp

        return (
        <Background home={require('../Assets/home.png')} left={require('../Assets/back.png')}
        leftNavigation={this.props.navigation}
        navigation={this.props.screenProps.SignedOutNavigation}
         >
            {/* <SilverBox paddingBottom={20} paddingVertical={15}>
                    <Text style={{color: '#3E86A3', fontSize: 20, fontWeight: '800'}}>DETAILS</Text> */}

{/* <View style={{height:550,width:'100%'}}>
<Barcode value='012000018794' format="UPCE" background={'red'}
            lineColor={'black'} height={150} width={250} text='012000018794'
            />

</View> */}

                    {/* <MyWebView
                                 source={{uri: this.props.navigation.state.params.item.view_detail}}
                                 style={{width: '100%' ,marginTop: 5}}
                              /> */}
            {/* </SilverBox> */}


            <View style={{width:'96%',height:viewHeight,margin:'2%',shadowColor: 'rgba(75,76,77,0.75)',backgroundColor:'#E5E5E5'}}>
                    <Text style={{color: '#3E86A3', fontSize: 20, fontWeight: '800',marginLeft:'3%'}}>DETAILS</Text>


            <ImageBackground source={require('../Assets/playav-bg.png')}    
                resizeMode='stretch'                           
                style={{flex:1,margin:'2%'
                }}> 

                <View style={{width:'100%',flexDirection:'row',height:'18%',marginTop:'1%'}}>
                        <Image style={{  width: '20%', height: '90%',margin:'1%' }}
                                        source={{ uri: this.props.navigation.state.params.item.logo ,cache: 'force-cache'}}
                                        resizeMode='contain'/>
                         <Text style={{color: '#3E86A3', fontSize: 30, fontWeight: 'bold',textAlign:"right",width:'70%'}} numberOfLines={2}>{this.props.navigation.state.params.item.offer}</Text>         
                </View>

                        <View style={{width:'100%',flexDirection:'row',height:'15%',marginTop:'1%'}}>
                           <Text style={{color: '#3E86A3', fontSize: 16, fontWeight: '400',marginLeft:'3%',textAlign:'center'}}>{this.props.navigation.state.params.item.description}</Text>
                        </View>

                        <View style={{width:'100%',flexDirection:'row',height:'40%',marginTop:'1%',marginBottom:'1%'}}>
                        <Image style={{  width: '100%', height: '98%',margin:'1%' }}
                                 source={{ uri: this.props.navigation.state.params.item.image ,cache: 'force-cache'}}
                                resizeMode='contain'/>
                </View>

<ViewMoreText style={{width:'100%',flexDirection:'row',height:'17%',marginTop:5}}
          numberOfLines={2}
          renderViewMore={this.renderViewMore}
          renderViewLess={this.renderViewLess}
          afterExpand={()=>{
              //alert('after expand')
            this.setState({isMoreView:false})
          }}
          afterCollapse={()=>{
             // alert('after collapse')
            this.setState({isMoreView:true})
        }}
          textStyle={{textAlign: 'left',marginHorizontal:10}}
        >
          <Text style={{color: '#000000', fontSize: 14, fontWeight: '300',marginLeft:'3%',textAlign:'left'}}>{this.props.navigation.state.params.item.detail}
        </Text>
        </ViewMoreText>
                {/* <View style={{width:'100%',flexDirection:'row',height:'19%',marginTop:'1%'}}>
                <Text style={{color: '#3E86A3', fontSize: 16, fontWeight: '400',marginLeft:'3%',textAlign:'center'}} numberOfLines={2}>{this.props.navigation.state.params.item.detail}</Text>
                </View> */}

                <View style={{width:'100%',flexDirection:'row',height:'6%'}}>
                <Text style={{color: '#3E86A3', fontSize: 14, fontWeight: '400',textAlign:"right",width:'97%'}} numberOfLines={1}>{this.props.navigation.state.params.item.end_date}</Text>
                </View>
                
                </ImageBackground>
        </View>

            <View style={{marginTop: '1%',width:'100%',alignItems:'center'}}>
            <Button name={'PLAY GAME'} paddingHorizontal={10} fontSize={PixelRatio.get() * 6}
                        onPress={() => this.playGame()}/>
            </View>
        </Background>
    )
};
}