import React, {Component} from 'react';
import {Image,
     Text,
      View,
      ImageBackground,
      Dimensions,
    } from 'react-native';
import * as Animatable from 'react-native-animatable';
import Background from '../Components/background';
import SilverBox from '../Components/silver_box';
import { Barcode, Formats } from 'react-native-1d-barcodes';
import ViewMoreText from 'react-native-view-more-text';


export default class RedeemedCoupon extends Component {

    constructor(props) {
        super();
        this.state = {
            barCodeWidth:300,
            barCodeHeight:100,
            isMoreView:true
        }
        let WebViewRef;
    }
    componentDidMount() {
        // setTimeout(() => {
        //     this.refs.aniimage.flipInX();
        //     this.refs.anibarcode.flipInY();
        // }, 1)
    }
    renderViewMore(onPress){
        return(
          <Text style={{color:'#3E86A3',marginLeft:10}} onPress={onPress}>View more</Text>
        )
      }
      renderViewLess(onPress){
        return(
          <Text style={{color:'#3E86A3',marginLeft:10}} onPress={onPress}>View less</Text>
        )
      }

    render() {

        const iphoneHeight = Dimensions.get('window').height
         //var str = 'jhgjbvjbbbvjhbdfbvduuujfjvbadgjlhlgldhglidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivhjhgjbvjbbbvjhbdfbvduuujfjvbadgjlhlgldhglidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivhjhgjbvjbbbvjhbdfbvduuujfjvbadgjjgjkgjkghvjhlidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivh'
         var countstr = this.props.navigation.state.params.item.detail.length
         var temp
         if (!this.state.isMoreView){
         if (countstr > 400){
           temp = 700
         }else if(countstr > 300){
            temp = 650
         }else if(countstr > 200){
          temp = 600
         }else if(countstr > 100){
         temp = 550
         }else{
             temp = 500
         }
     }
         var viewHeight = (this.state.isMoreView) ? (iphoneHeight * 65) / 100 : temp
        
        return (
            <Background home={require('../Assets/home.png')} left={require('../Assets/back.png')} navigation={this.props.navigation}
            >


 <View style={{width:'96%',height:viewHeight,margin:'2%',shadowColor: 'rgba(75,76,77,0.75)',backgroundColor:'#E5E5E5'}}>
                    <Text style={{color: '#3E86A3', fontSize: 20, fontWeight: '800',marginLeft:'3%'}}>DETAILS</Text>

    <ImageBackground source={require('../Assets/playav-bg.png')}    
                resizeMode='stretch'                           
                style={{flex:1,margin:'2%'
                }}> 

                <View style={{width:'100%',flexDirection:'row',height:'15%',marginTop:'1%'}}>
                <Image style={{  width: '20%', height: '90%',margin:'0.5%' }}
                                 source={{ uri: this.props.navigation.state.params.item.logo ,cache: 'force-cache'}}
                                resizeMode='contain'/>
                <Text style={{color: '#3E86A3', fontSize: 30, fontWeight: 'bold',textAlign:"right",width:'70%'}} numberOfLines={2}>{this.props.navigation.state.params.item.offer}</Text>         
                </View>

                <View style={{width:'100%',flexDirection:'row',height:'13%',marginTop:'1%'}}>
                <Text style={{color: '#3E86A3', fontSize: 16, fontWeight: '400',marginLeft:'3%',textAlign:'center'}}>{this.props.navigation.state.params.item.description}</Text>
                </View>

                <View style={{width:'100%',flexDirection:'row',height:'30%',marginTop:'1%',marginBottom:'1%'}}>
                <Image style={{  width: '100%', height: '98%',margin:'1%' }}
                                 source={{ uri: this.props.navigation.state.params.item.image ,cache: 'force-cache'}}
                                resizeMode='contain'/>
                </View>

<ViewMoreText style={{width:'100%',flexDirection:'row',height:'17%',marginTop:5}}
          numberOfLines={2}
          renderViewMore={this.renderViewMore}
          renderViewLess={this.renderViewLess}
          afterExpand={()=>{
              //alert('after expand')
            this.setState({isMoreView:false})
          }}
          afterCollapse={()=>{
             // alert('after collapse')
            this.setState({isMoreView:true})
        }}
          textStyle={{textAlign: 'left',marginHorizontal:10}}
        >
          <Text style={{color: '#000000', fontSize: 14, fontWeight: '300',marginLeft:'3%',textAlign:'left'}}>{this.props.navigation.state.params.item.detail}
        </Text>
        </ViewMoreText>
                {/* <View style={{width:'100%',flexDirection:'row',height:'17%',marginTop:'1%'}}>
                <Text style={{color: '#3E86A3', fontSize: 14, fontWeight: '400',marginLeft:'3%',textAlign:'center'}} numberOfLines={2}>{this.props.navigation.state.params.item.detail}</Text>
                </View> */}

                <View style={{width:'100%',flexDirection:'row',height:'16%',marginTop:'1%',justifyContent:'center'}} onLayout={(event) => {
                    var {x, y, width, height} = event.nativeEvent.layout;
                    var barW = (width * 65) / 100;
                     this.setState({barCodeHeight:height,barCodeWidth:barW})
                    }}>
                

            {!(this.props.navigation.state.params.from === "saving") ?(this.props.navigation.state.params.item.code_type === 'upc') ? (<Barcode
                bgColor={'#FFFFFF'}
                fgColor={'#000000'}
                format={Formats.UPC_A}
                value={this.props.navigation.state.params.item.promo_code}
                width={this.state.barCodeWidth}
                height={this.state.barCodeHeight}
        />) : (
            <View style={{width:'70%',flexDirection:'column',height:this.state.barCodeHeight, marginTop:'2%',justifyContent:'center'}}>
                <Text style={{color: '#3E86A3', fontSize: 20, fontWeight: '400',width:'97%',textAlign:'center'}} numberOfLines={1}>{this.props.navigation.state.params.item.promo_code}</Text>
          </View>
        ):<View style={{width:'70%',flexDirection:'column',backgroundColor:"#999999",height:this.state.barCodeHeight, marginTop:'2%',justifyContent:'center'}}>
                {/*<Text style={{color: '#3E86A3', fontSize: 20, fontWeight: '400',width:'97%',textAlign:'center'}} numberOfLines={1}>{this.props.navigation.state.params.item.promo_code}</Text>*/}
            </View>}
         <View style={{width:'27%',flexDirection:'column',height:'100%', marginTop:'3%'}}>
                <Text style={{color: '#3E86A3', fontSize: 16, fontWeight: '400',width:'97%',textAlign:'center'}} numberOfLines={1}>Expires</Text>
                <Text style={{color: '#3E86A3', fontSize: 14, fontWeight: '400',textAlign:"right",width:'97%'}} numberOfLines={2}>{this.props.navigation.state.params.item.end_date}</Text>

        </View>
                </View>

                
                </ImageBackground>

</View>


                {/* <SilverBox paddingBottom={30} paddingVertical={20}>
                <MyWebView
                                 source={{uri: (this.props.navigation.state.params.item.view_detail !== null)? this.props.navigation.state.params.item.view_detail :null}}
                                 style={{width: '100%' ,marginTop: 5}}
                              /> */}
                    {/* <View style={{flexDirection: 'row'}}>
                        <Image source={require('../Assets/mcdonalds-logo.png')}/>
                        <Text style={{color: '#6D6D6D', fontSize: 26, marginLeft: 10}}>McDonald's</Text>
                    </View>
                    <Animatable.Image useNativeDriver ref={'aniimage'} source={require('../Assets/coupon-banner.png')} resizeMode={'contain'} style={{width: null}}/>
                    <Text style={{color: '#6D6D6D', fontSize: 20}}>MCDONALDS 25% OFF</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 20}}>
                        <View>
                            <Text style={{color: '#3E86A3', fontWeight: 'bold'}}>Valid from</Text>
                            <Text style={{color: '#6D6D6D', fontSize: 18}}>15 APRIL 2018</Text>
                        </View>
                        <View>
                            <Text style={{color: '#3E86A3', fontWeight: 'bold'}}>Valid to</Text>
                            <Text style={{color: '#6D6D6D', fontSize: 18}}>15 MAY 2018</Text>
                        </View>
                    </View>
                    <Animatable.Image useNativeDriver ref={'anibarcode'} source={require('../Assets/barcode.png')} style={{alignSelf: 'center', marginTop: 20}}/> */}
                {/* </SilverBox> */}
            </Background>
        )
    }
};