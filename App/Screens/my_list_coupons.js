import React, {Component} from 'react';
import {
    Text,
    Image,
    View,
    FlatList,
    Animated,
    TouchableOpacity,
    Modal,
    TouchableWithoutFeedback,
    Dimensions,
    ActivityIndicator
} from 'react-native';
import Background from '../Components/background';
import GradientSilverBox from '../Components/gradient_silver_box';
import api from '../api';
import Loader from '../Components/activityIndicator'
import navigate from '../Components/navigate';
import firebase from 'react-native-firebase';


// const data = [
//     {head: 'Mcdonalds 25% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Pepsi 30% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: "Cabela's 25% off", text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Old navy 30% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Mcdonalds 25% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Pepsi 30% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Old navy 30% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'}
// ];


export default class MyListCoupons extends Component {
    constructor() {
        super();
        this.onEndReachedCalledDuringMomentum = true;
        this.state = {
            opacity: new Animated.Value(0),
            couponList: [],
            modalVisible: false,
            scrollPosition: 0,
            totalPages: 1,
            pageNo: 1
        }
        firebase.analytics().setCurrentScreen('My List');
    }

    componentDidMount() {
        Animated.timing(
            this.state.opacity, {toValue: 1, duration: 1000}
        ).start();
        this.getCouponList();
    }

    componentWillUnmount() {
        console.log(this.props.navigation.state.params.delegate.getList())
    }

    getCouponList = async () => {
        var _this = this
        this.setState({loading: true});

        try {
            let response = await api.getRequestApi("list/" + this.props.navigation.state.params.item.id + '/1');
            console.log('--------------- get Coupon List response-----------')
            console.log(response)
            this.setState({loading: false});
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        console.log(JSON.stringify(data))
                        _this.setState({couponList: data.coupons, totalPages: data.pages})
                    })
                } catch (error) {
                    console.log(error)
                }
            }
        } catch (error) {
            this.setState({loading: false});
            console.log(error)
        }
    }


    removeFromList = async (index) => {
        var _this = this
        this.setState({loading: true});
        let body = {
            "win_id": this.state.couponList[index].win_id,
            "list_id": this.props.navigation.state.params.item.id
        };
        console.log('----', body)
        try {
            let response = await api.request('remove-from-list', 'delete', body);
            console.log('--------------- Remove Coupon from List response-----------')
            console.log(response)
            this.setState({loading: false});
            if (response.status === 200) {
                _this.getCouponList()
            }
        } catch (error) {
            this.setState({loading: false});
            console.log(error)
        }

    }

    fetchCouponList = async () => {
        var _this = this
        this.setState({loading: true});

        try {
            let response = await api.getRequestApi("list/" + this.props.navigation.state.params.item.id + '/' + this.state.pageNo);
            this.setState({loading: false});
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        _this.setState({couponList: this.state.couponList.concat(data.coupons)})
                    })
                } catch (error) {
                    console.log(error)
                }
            }
        } catch (error) {
            this.setState({loading: false});
            console.log(error)
        }
    }


    onEndReached() {
        console.log('called')

        if (!this.onEndReachedCalledDuringMomentum) {
            console.log('called')
            this.onEndReachedCalledDuringMomentum = true;
            this.setState({pageNo: this.state.pageNo + 1})
            if (this.state.pageNo > this.state.totalPages) {
                return;
            }
            this.fetchCouponList()
        }
    }

    renderFooter = () => {
        if (!this.state.loading) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                }}
            >
                <ActivityIndicator animating size="large" color='rgb(254,254,254)'/>
            </View>
        );
    };

    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity onPress={() => navigate.navigateTo(this.props.navigation, 'CouponDetail', {
                item: item,
                delegate: this
            })}>
                <Animated.View useNativeDriver style={{
                    marginHorizontal: 15,
                    marginTop: 5,
                    borderWidth: 2,
                    borderColor: '#525252',
                    borderStyle: 'dashed',
                    borderRadius: 5,
                    opacity: this.state.opacity
                }}>
                    <GradientSilverBox>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{width: '25%', alignItems: 'flex-start', justifyContent: 'center'}}>
                                {/* <Image source={require('../Assets/mcdonalds-logo.png')}/> */}
                                <Image style={{width: 55, height: 55, marginLeft: 1, marginTop: 2}}
                                       source={{uri: item.logo, cache: 'force-cache'}}
                                       resizeMode="contain"/>
                            </View>
                            <View style={{width: '52%'}}>
                                <Text style={{color: '#000000', fontSize: 18, fontWeight: 'bold'}}>{item.title}</Text>
                                <Text numberOfLines={2}>{item.description}</Text>
                            </View>
                            <TouchableOpacity
                                style={{height: '100%', width: '20%', marginleft: '10%', alignItems: 'center'}}
                                onPress={() => this.setState({modalVisible: true, index})}>
                                <Image
                                    style={{marginTop: '20%'}}
                                    resizeMode='contain'
                                    source={require('../Assets/option.png')}/>
                                {/* </View> */}
                            </TouchableOpacity>
                            {/* <View style={{width: '10%', alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('../Assets/cpn-arrow.png')}/>
                        </View> */}
                        </View>
                    </GradientSilverBox>
                </Animated.View>
            </TouchableOpacity>
        )
    };

    modal() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <TouchableWithoutFeedback onPress={() => this.setState({modalVisible: false})}>
                    <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.1)'}}>
                        <View style={{
                            backgroundColor: '#909090',
                            borderRadius: 7,
                            width: 100,
                            padding: 10,
                            alignSelf: 'flex-end',
                            top: ((Dimensions.get('window').width * 0.7) + (this.state.index * 65) - (this.state.scrollPosition)),
                            right: '12%'
                        }}>
                            <View style={{flexDirection: 'row'}}>
                                {/* <Image source={require('../Assets/trash.png')}/> */}
                                <TouchableOpacity onPress={() => {
                                    this.removeFromList(this.state.index);
                                    this.setState({modalVisible: false})
                                }}>
                                    <Text style={{marginLeft: 10}}>Remove</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }

    handleScroll = (event) => {
        this.setState({scrollPosition: event.nativeEvent.contentOffset.y})
    };

    render() {
        return (
            <Background withoutSroll={true} home={require('../Assets/home.png')} left={require('../Assets/back.png')}
                        leftNavigation={this.props.navigation}
                        navigation={this.props.screenProps.SignedOutNavigation}
            >
                <Text style={{
                    color: 'white',
                    fontSize: 18,
                    marginVertical: 20,
                    alignItems: 'center',
                    width: '100%',
                    textAlign: 'center'
                }}>{this.props.navigation.state.params.item.name}</Text>
                <View style={{width: '100%'}}>
                    <FlatList
                        ListFooterComponent={this.renderFooter}
                        style={{marginBottom: 62}}
                        data={this.state.couponList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this.renderItem}
                        onEndReachedThreshold={0.5}
                        onEndReached={() => this.onEndReached()}
                        onMomentumScrollBegin={() => {
                            this.onEndReachedCalledDuringMomentum = false;
                        }}

                    />
                </View>
                {this.modal()}
                {/* {this.state.loading ?
                <Loader/> : null
                } */}
            </Background>
        )
    }
}
