import React, {Component} from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet,
    PixelRatio,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    Modal,
    TouchableWithoutFeedback,
    AsyncStorage,
    KeyboardAvoidingView,
    ActivityIndicator,
    InteractionManager,
    TextInput,
    Keyboard
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import firebase from 'react-native-firebase';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import navigate from '../Components/navigate';
import Background from '../Components/background';
import GradientSilverBox from '../Components/gradient_silver_box';
import TextBox from '../Components/text_box';
import SilverBox from '../Components/silver_box';
import Button from '../Components/button';
import api from '../api';


export default class Dashboard extends Component {
    constructor() {
        super();
        this.state = {
            modalVisible: false,
            resetPasswordModel: false,
            submitFeedbackModel: false,
            oldPassword: '',
            newPassword: '',
            reTypePassword: '',
            feedbackMessage: '',
            error: '',
            loading: false,
            isGuest:false
        };
        firebase.analytics().setCurrentScreen('Dashboard');
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            try {
                this.refs.tab1.zoomIn();
                this.refs.tab2.zoomIn();
                // this.refs.tab3.zoomIn();
                this.refs.tab4.zoomIn();
                this.refs.tab5.zoomIn();
                this.refs.tab6.zoomIn();
            } catch (e) {
            }
        });
        this.getUserType();
    }

    async getUserType(){
        var isGuest = await AsyncStorage.getItem('isGuest')
        if(isGuest==='1'){
            this.setState({ isGuest: true})
        }
        console.log(" is Guest", isGuest)
    }

    openModal() {
        this.setState({modalVisible: true})
    }

    logout() {
        AsyncStorage.clear();
        navigate.navigateWithReset(this.props.screenProps.rootNavigation, 'SignedOut')
    }

    modal() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <TouchableWithoutFeedback onPress={() => this.setState({modalVisible: false})}>
                    <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.1)'}}>
                        <View style={{
                            backgroundColor: '#909090', borderRadius: 7, width: 170, padding: 10, alignSelf: 'flex-end',
                            top: '11%', right: '5%'
                        }}>

                            {/* <View style={{flexDirection: 'row'}}> */}
                            <TouchableOpacity onPress={() => {
                                this.setState({submitFeedbackModel: true, modalVisible: false})
                            }}>
                                <Text style={{marginLeft: 10, fontSize: 16}}>Submit Feedback</Text>
                            </TouchableOpacity>
                            { this.state.isGuest ? (null) : (
                                <TouchableOpacity onPress={() => {
                                    this.setState({resetPasswordModel: true, modalVisible: false})
                                }}>
                                    <Text style={{marginLeft: 10, fontSize: 16, marginTop: 10}}>Change password</Text>
                                </TouchableOpacity>
                            )}
                            
                             {/* <TouchableOpacity onPress={() => {
                                this.setState({resetPasswordModel: true, modalVisible: false})
                            }}>
                                <Text style={{marginLeft: 10, fontSize: 16, marginTop: 10}}>Change password</Text>
                            </TouchableOpacity> */}

                            <TouchableOpacity onPress={() => {
                                this.logout()
                            }}>
                                <Text style={{marginLeft: 10, marginTop: 10, fontSize: 16}}>Logout</Text>
                            </TouchableOpacity>
                            {/* </View> */}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }

    updatePassword = async () => {
        const {oldPassword, newPassword, reTypePassword} = this.state
        if (oldPassword.length < 6) {
            this.setState({error: 'Old Password must contain 6 characters.'})
            return
        }
        if (newPassword.length < 6) {
            this.setState({error: 'New Password must contain 6 characters.'})
            return
        }
        if (reTypePassword.length < 6) {
            this.setState({error: 'Re Type Password must contain 6 characters.'})
            return
        }
        if (newPassword !== reTypePassword) {
            this.setState({error: 'New password and Re Type Password not matched'})
            return
        }
        this.setState({loading: true});
        let body = {
            "old_password": oldPassword,
            "password": reTypePassword
        };
        try {
            let response = await api.request('reset-password', 'post', body);

            if (response.status === 200) {
                this.setState({loading: false, resetPasswordModel: false});
                try {
                    await response.json().then((data) => {
                        alert(data.msg)
                    });
                } catch (error) {
                    console.log(error);
                    this.setState({loading: false});
                    alert("There was an error processing your data. Please try again." + response.status)
                }
            } else {
                this.setState({loading: false, resetPasswordModel: false});
                await response.json().then((res) => {
                    console.log(res)
                    alert(res.errors)
                });
            }
        } catch (error) {
            console.log(error);
            this.setState({loading: false, resetPasswordModel: false});
            alert("There was an error processing your data. Please try again." + response.status)
        }
    }

// SubmitFeedBack Api call
    submitFeedBack = async () => {
        if (this.state.feedbackMessage.length === 0) {
            this.setState({error: 'Please type Feedback.'})
            return
        }
        this.setState({loading: true});
        let body = {
            "feedback": this.state.feedbackMessage,
        };
        try {
            let response = await api.request('user-feedback', 'post', body);
            if (response.status === 200) {
                this.setState({loading: false, submitFeedbackModel: false});
                try {
                    await response.json().then((data) => {
                        setTimeout(function () {
                            alert(data.message)
                        }, 500);
                    });
                } catch (error) {
                    console.log(error);
                    this.setState({loading: false});
                    alert("There was an error processing your data. Please try again." + response.status)
                }
            } else {
                this.setState({loading: false, submitFeedbackModel: false});
                await response.json().then((res) => {
                    console.log(res)
                    alert(res.errors + response.status)
                });
            }
        } catch (error) {
            console.log(error);
            this.setState({loading: false, submitFeedbackModel: false});
            alert("There was an error processing your data. Please try again." + response.status)
        }
    }


// Change Password model
    renderModalContent() {
        return (
            <View style={{width: '85%'}}>
                <View style={{width: '111%'}}>
                    <SilverBox>
                        <View style={{flexDirection: 'column', justifyContent: 'center', width: '100%'}}>
                            <View>
                                <Text style={{
                                    fontSize: 16,
                                    color: '#000000',
                                    fontWeight: '600',
                                    marginVertical: 10,
                                    alignSelf: 'center'
                                }}>
                                    Change your password </Text></View>
                            <View>
                                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                    <Text style={{color: 'red'}}>
                                        {this.state.error ? this.state.error : this.state.errors}
                                    </Text>
                                </View>

                                {/*<KeyboardAvoidingView behavior="padding" enabled>*/}
                                <Animatable.View useNativeDriver ref={'oldPassword'}>
                                    <TextBox placeholder={'Old Password'}
                                             icon={require('../Assets/Passsword.png')}
                                             secureTextEntry
                                             returnKeyType={'next'}
                                             reference={'oldPasswordtf'}
                                             ref={'oldPasswordtf'}
                                             onChangeText={(oldPassword) => this.setState({oldPassword})}
                                             onSubmitEditing={() => this.refs.newPasswordtf.refs.newPasswordtf.focus()}
                                    />
                                </Animatable.View>

                                <Animatable.View useNativeDriver ref={'newPassword'}>
                                    <TextBox placeholder={'New Password'}
                                             icon={require('../Assets/Passsword.png')}
                                             secureTextEntry
                                             returnKeyType={'next'}
                                             reference={'newPasswordtf'}
                                             ref={'newPasswordtf'}
                                             onChangeText={(newPassword) => this.setState({newPassword})}
                                             onSubmitEditing={() => this.refs.reTypePasswordtf.refs.reTypePasswordtf.focus()}
                                    />
                                </Animatable.View>

                                <Animatable.View useNativeDriver ref={'reTypePassword'}>
                                    <TextBox placeholder={'Retype New Password'}
                                             icon={require('../Assets/Passsword.png')}
                                             secureTextEntry
                                             returnKeyType={'done'}
                                             reference={'reTypePasswordtf'}
                                             ref={'reTypePasswordtf'}
                                             onChangeText={(reTypePassword) => this.setState({reTypePassword})}
                                    />
                                </Animatable.View>

                                {/*</KeyboardAvoidingView>*/}
                            </View>
                            <View style={{position: 'relative', alignSelf: 'center', zIndex: 3, top: 40, height: 90}}>
                                <Button name={'UPDATE'} paddingHorizontal={80}
                                        onPress={() => {
                                            this.updatePassword()
                                        }}/>

                            </View>
                            {this.state.loading &&
                            <View style={{
                                flex: 1, position: 'absolute', left: 0,
                                right: 0, top: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'
                            }}>
                                <ActivityIndicator size="large" color='rgb(54,122,223)'/>
                            </View>
                            }
                        </View>
                    </SilverBox>
                </View>
            </View>
        )
    }

// Submit FeedBack Model
    renderSubmitFeedbackModalContent() {
        return (
            <View style={{width: '85%'}}>
                <View style={{width: '111%'}}>
                    <SilverBox>
                        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                            <View style={{flexDirection: 'column', justifyContent: 'center', width: '100%'}}>
                                <View>
                                    <Text style={{
                                        fontSize: 16,
                                        color: '#000000',
                                        fontWeight: '600',
                                        marginVertical: 10,
                                        alignSelf: 'center'
                                    }}>
                                        Submit Your Feedback </Text></View>
                                <View>
                                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                        <Text style={{color: 'red'}}>
                                            {this.state.error ? this.state.error : this.state.errors}
                                        </Text>
                                    </View>

                                    <View style={{
                                        backgroundColor: 'white', borderRadius: 5, margin: '5%', shadowColor: '#999666',
                                        shadowOffset: {width: 1, height: 1}, shadowOpacity: 0.2, elevation: 2
                                    }}>

                                        <Text style={{marginTop: 20, marginLeft: 20, color: '#6A6A6A'}}>Message</Text>
                                        <TextInput
                                            multiline={true}
                                            textAlignVertical='top'
                                            returnKeyType={'next'}
                                            placeholder="Write your feedback"
                                            style={{
                                                margin: 10,
                                                borderColor: '#E9E9E9',
                                                borderWidth: 1,
                                                height: 200,
                                                borderRadius: 5
                                            }}
                                            underlineColorAndroid='transparent'
                                            onChangeText={(feedbackMessage) => this.setState({feedbackMessage})}
                                        />
                                    </View>

                                </View>
                                <View
                                    style={{position: 'relative', alignSelf: 'center', zIndex: 3, top: 40, height: 90}}>
                                    <Button name={'SUBMIT'} paddingHorizontal={80}
                                            onPress={() => {
                                                this.submitFeedBack()
                                            }}/>

                                </View>
                                {this.state.loading &&
                                <View style={{
                                    flex: 1, position: 'absolute', left: 0,
                                    right: 0, top: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'
                                }}>
                                    <ActivityIndicator size="large" color='rgb(54,122,223)'/>
                                </View>
                                }
                            </View>
                        </TouchableWithoutFeedback>
                    </SilverBox>
                </View>
            </View>
        )
    }


    render() {
        const {height, width} = Dimensions.get('window');
        return (
            <Background
                openModal={this.openModal.bind(this)}
                settings={require('../Assets/setting.png')}>
                {this.modal()}
                <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '90%', marginTop: 50}}>
                    <TouchableOpacity onPress={() => {
                        navigate.navigateTo(this.props.navigation, 'GameTypes')
                    }} style={styles.boxView}>
                        <Animatable.View useNativeDriver ref={'tab1'}>
                            <GradientSilverBox>
                                <Image source={require('../Assets/game-dashord.png')}/>
                            </GradientSilverBox>
                            <Text style={styles.text}>Games</Text>
                        </Animatable.View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        navigate.navigateTo(this.props.navigation, 'CouponsTab')
                    }} style={styles.boxView}>
                        <Animatable.View useNativeDriver ref={'tab2'}>
                            <GradientSilverBox>
                                <Image source={require('../Assets/coupon.png')}/>
                            </GradientSilverBox>
                            <Text style={styles.text}>My Rewards</Text>
                        </Animatable.View>
                    </TouchableOpacity>

                    {/* <TouchableOpacity onPress={() => {
                        navigate.navigateTo(this.props.navigation, 'Spotlight')
                    }} style={styles.boxView}>
                        <Animatable.View useNativeDriver ref={'tab3'}>
                            <GradientSilverBox>
                                <Image source={require('../Assets/spot-lite.png')}/>
                            </GradientSilverBox>
                            <Text style={styles.text}>Spotlight</Text>
                        </Animatable.View>
                    </TouchableOpacity> */}
                    <TouchableOpacity onPress={() => {
                        navigate.navigateTo(this.props.navigation, 'Savings')
                    }} style={styles.boxView}>
                        <Animatable.View useNativeDriver ref={'tab4'}>
                            <GradientSilverBox>
                                <Image source={require('../Assets/us-dollar.png')}/>
                            </GradientSilverBox>
                            <Text style={styles.text}>Savings</Text>
                        </Animatable.View>
                    </TouchableOpacity>
                </View>
                {/* Second Row */}

                <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '60%', marginTop: 40}}>

                    {/* <TouchableOpacity onPress={() => {
                        navigate.navigateTo(this.props.navigation, 'Savings')
                    }} style={styles.boxView}>
                        <Animatable.View useNativeDriver ref={'tab4'}>
                            <GradientSilverBox>
                                <Image source={require('../Assets/us-dollar.png')}/>
                            </GradientSilverBox>
                            <Text style={styles.text}>Savings</Text>
                        </Animatable.View>
                    </TouchableOpacity> */}

                    <TouchableOpacity onPress={() => {
                        navigate.navigateTo(this.props.navigation, 'MyList')
                    }} style={{width: '46.5%'}}>
                        <Animatable.View useNativeDriver ref={'tab5'}>
                            <GradientSilverBox>
                                <Image source={require('../Assets/my-list.png')}/>
                            </GradientSilverBox>
                            <Text style={styles.text}>My List</Text>
                        </Animatable.View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        navigate.navigateWithReset(this.props.navigation, 'InterestSlides')
                    }} style={{width: '46.5%'}}>
                        <Animatable.View useNativeDriver ref={'tab6'}>
                            <GradientSilverBox>
                                <Image source={require('../Assets/user-dashborad.png')}/>
                            </GradientSilverBox>
                            <Text style={styles.text}>Profile</Text>
                        </Animatable.View>
                    </TouchableOpacity>
                </View>

                {/* change Password View */}
                <Modal animationType="fade" transparent={true} visible={this.state.resetPasswordModel ? true : false}>
                    <KeyboardAwareScrollView behavior="padding" enabled style={{flex: 1, height: height}}>
                        <TouchableWithoutFeedback onPress={() => {
                            this.setState({resetPasswordModel: false, error: ''})
                        }}>
                            <View style={{
                                flex: 1,
                                height: height,
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: 'rgba(0,0,0,0.67)'
                            }}>
                                <TouchableWithoutFeedback>
                                    {this.renderModalContent()}
                                </TouchableWithoutFeedback>
                            </View>
                        </TouchableWithoutFeedback>
                    </KeyboardAwareScrollView>
                </Modal>

                {/* Submit Feedback */}
                <Modal animationType="fade" transparent={true} visible={this.state.submitFeedbackModel ? true : false}>
                    <KeyboardAwareScrollView enabled style={{flex: 1, height: height}}>
                        <TouchableWithoutFeedback onPress={() => {
                            this.setState({submitFeedbackModel: false, error: ''})
                        }}>
                            <View style={{
                                flex: 1,
                                height: height,
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: 'rgba(0,0,0,0.67)'
                            }}>
                                <TouchableWithoutFeedback>
                                    {this.renderSubmitFeedbackModalContent()}
                                </TouchableWithoutFeedback>
                            </View>
                        </TouchableWithoutFeedback>
                    </KeyboardAwareScrollView>
                </Modal>


                {/* <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                            ref='scroller'
                            style={{marginTop: height * 0.1, width: '100%'}}>
                    <View>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Image style={{marginHorizontal: 20}} source={require('../Assets/mcdonalds-logo.png')}/>
                            <Image style={{marginHorizontal: 20}} source={require('../Assets/old-navy-logo.png')}/>
                            <Image style={{marginHorizontal: 20}} source={require('../Assets/pepsi-logo.png')}/>
                            <Image style={{marginHorizontal: 20}} source={require('../Assets/cabelas-logo.png')}/>
                            <Image style={{marginHorizontal: 20}} source={require('../Assets/mcdonalds-logo.png')}/>
                            <Image style={{marginHorizontal: 20}} source={require('../Assets/old-navy-logo.png')}/>
                            <Image style={{marginHorizontal: 20}} source={require('../Assets/pepsi-logo.png')}/>
                            <Image style={{marginHorizontal: 20}} source={require('../Assets/cabelas-logo.png')}/>
                        </View>
                        <View style={{flexDirection: 'row', opacity: 0.2, marginTop: 10, alignItems: 'center'}}>
                            <Image style={styles.mirrorLogo} source={require('../Assets/mcdonalds-logo.png')}/>
                            <Image style={styles.mirrorLogo} source={require('../Assets/old-navy-logo.png')}/>
                            <Image style={styles.mirrorLogo} source={require('../Assets/pepsi-logo.png')}/>
                            <Image style={styles.mirrorLogo} source={require('../Assets/cabelas-logo.png')}/>
                            <Image style={styles.mirrorLogo} source={require('../Assets/mcdonalds-logo.png')}/>
                            <Image style={styles.mirrorLogo} source={require('../Assets/old-navy-logo.png')}/>
                            <Image style={styles.mirrorLogo} source={require('../Assets/pepsi-logo.png')}/>
                            <Image style={styles.mirrorLogo} source={require('../Assets/cabelas-logo.png')}/>
                        </View>
                    </View>
                </ScrollView> */}

            </Background>
        )
    }
}

const styles = StyleSheet.create({
    text: {color: 'white', fontSize: PixelRatio.get() * 5, fontWeight: 'bold', alignSelf: 'center', marginTop: 10},
    boxView: {width: '30%'},
    mirrorLogo: {marginHorizontal: 20, transform: [{rotateX: '180deg'}]}
});
