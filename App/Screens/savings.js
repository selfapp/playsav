import React, {Component} from 'react';
import {FlatList,
     Text,
      View,
       Image,
        ImageBackground,
        TouchableOpacity,
        ActivityIndicator
    } from 'react-native';
import Background from '../Components/background';
import GradientSilverBox from '../Components/gradient_silver_box';
import api from '../api';
import Loader from '../Components/activityIndicator'
import navigate from '../Components/navigate';
import firebase from 'react-native-firebase';

// const data = [
//     {head: 'Mcdonalds 25% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Pepsi 30% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: "Cabela's 25% off", text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Old navy 30% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Mcdonalds 25% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Pepsi 30% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'},
//     {head: 'Old navy 30% off', text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'}
// ];

    export default class Savings extends Component {

        constructor() {
            super();
            this.onEndReachedCalledDuringMomentum = true;

            this.state = {
                redeemCoupon:[],
                loading: false,
                totalPages:1,
                pageNo: 1
            }
            firebase.analytics().setCurrentScreen('Savings');

        }

        componentDidMount(){
            this.getRedeemCoupon()
        }


        getRedeemCoupon = async () =>{
            var _this = this;
            this.setState({ loading: true });
            try {
                let response = await api.getRequestApi('my-redemeed-coupons-listing/1');
                console.log('--------------- get redeem Coupon response-----------');
                console.log(response);
                this.setState({ loading: false });
                if (response.status === 200) {
                    try {
                        await response.json().then((data) => {
                            _this.setState({redeemCoupon:data.coupons, totalPages: data.pages})
                        })
                    } catch (error) {
                        console.log(error)
                    }
                }
            } catch (error) {
                console.log(error)
            }
        };

        fetchCouponList = async () =>{
            var _this = this;
            this.setState({ loading: true });
            try {
                let response = await api.getRequestApi('my-redemeed-coupons-listing/' + this.state.pageNo);
                this.setState({ loading: false });
                if (response.status === 200) {
                    try {
                        await response.json().then((data) => {
                            _this.setState({redeemCoupon: this.state.redeemCoupon.concat(data.coupons)})
                        })
                    } catch (error) {
                        console.log(error)
                    }
                }
            } catch (error) {
                console.log(error)
            }
        };


        onEndReached(){
            if(!this.onEndReachedCalledDuringMomentum){
                console.log('called')
                this.onEndReachedCalledDuringMomentum = true;
                this.setState({ pageNo: this.state.pageNo + 1})
                if(this.state.pageNo > this.state.totalPages)
                {
                    return;
                }
                this.fetchCouponList()
            }
        }

        renderFooter = () => {
            if (!this.state.loading) return null;
            return (
              <View
                style={{
                  paddingVertical: 20,
                }}
              >
                <ActivityIndicator animating size="large" color='rgb(254,254,254)' />
              </View>
            );
          };

        renderItem = ({item}) => {
        return (
            <TouchableOpacity onPress={() => navigate.navigateTo(this.props.navigation, 'RedeemedCoupon',{item:item, from:"saving"})}>
            <View useNativeDriver style={{
                marginHorizontal: 15,
                marginTop: 5,
                borderWidth: 2,
                borderColor: '#525252',
                borderStyle: 'dashed',
                borderRadius: 5
            }}>
                <GradientSilverBox>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{width: '25%', alignItems: 'center', justifyContent: 'center'}}>
                            {/* <Image source={require('../Assets/mcdonalds-logo.png')}/> */}
                            <Image style={{  width: 55, height: 55,marginLeft:-10,marginTop:2 }}
                                 source={{ uri: item.logo ,cache: 'force-cache'}}
                                resizeMode="contain"/>
                        </View>
                        <View style={{width: '65%'}}>
                            <Text style={{color: '#000000', fontSize: 15, fontWeight: 'bold'}} numberOfLines={1}>{item.offer_text}</Text>
                            <Text numberOfLines={2}>{item.description}</Text>
                        </View>
                        <View style={{width: '10%', alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('../Assets/cpn-arrow.png')}/>
                        </View>
                    </View>
                </GradientSilverBox>
            </View>
            </TouchableOpacity>
        )
    };
render(){
    return (
        <Background withoutSroll={true} home={require('../Assets/home.png')} leftToDashboard={require('../Assets/back.png')}
        navigation={this.props.navigation} handleScroll={this.handleScroll}>
            <Text style={{color: 'white', fontSize: 18, marginVertical: 20, alignSelf: 'flex-start', marginLeft: 20}}>Exchanged
                rewards</Text>

            <FlatList
                 ListFooterComponent={this.renderFooter}
                data={this.state.redeemCoupon}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this.renderItem}
                onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                onEndReached={ () => this.onEndReached()}
                onEndReachedThreshold={0.5}
            />

                {/* {this.state.loading ?
                <Loader/> : null
                } */}
        </Background>
    )
    }
}
