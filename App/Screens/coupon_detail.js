import React, {Component} from 'react';
import {
    Image,
    Text,
    View,
    PixelRatio,
    Modal,
    Alert,
    StyleSheet,
    TextInput,
    Clipboard,
    Linking,
    TouchableOpacity, TouchableWithoutFeedback,
    KeyboardAvoidingView,
    FlatList, Keyboard, ImageBackground, Dimensions
} from 'react-native';
import {Barcode, Formats} from 'react-native-1d-barcodes';
import ViewMoreText from 'react-native-view-more-text';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import firebase from 'react-native-firebase';
import Background from '../Components/background';
import Button from '../Components/button';
import api from '../api';


export default class CouponDetail extends Component {

    constructor(props) {
        super();
        this.state = {
            modalVisible: false,
            allSelectedItemList: [],
            isRedeem: false,
            myList: [],
            listName: '',
            loading: false,
            item: props.navigation.state.params.item,
            barCodeWidth: 200,
            barCodeHeight: 100,
            isMoreView: true,
            isReedemdPress: false,
        }
        firebase.analytics().setCurrentScreen('Coupon Details');
        let WebViewRef;
    }

    componentDidMount() {
        console.log('Data from existing class ' + JSON.stringify(this.state.item));
        this.setState({isRedeem: (this.state.item.redeemed === 1) ? true : false});
        this.getList()
    }

    componentWillUnmount() {
        console.log(this.props.navigation.state.params.delegate.getCouponList('new'))
        console.log(this.props.navigation.state.params.delegate.getCouponList('expiring'))
        console.log(this.props.navigation.state.params.delegate.getCouponList('all'))
    }

    getList = async () => {
        var _this = this
        this.setState({loading: true});
        try {
            let response = await api.getRequestApi('my-list');
            console.log('--------------- get List response-----------' + response)
            this.setState({loading: false});
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        _this.setState({myList: data.list})
                        console.log('------------', this.state.myList)
                    })
                } catch (error) {
                    console.log(error)
                }
            }
        } catch (error) {
            this.setState({loading: false});
            console.log(error)
        }
    }

    createList = async () => {
        this.setState({loading: true});
        let body = {
            "name": this.state.listName
        };
        this.setState({listName: ''});
        try {
            let response = await api.request('add-list', 'post', body);
            console.log('-----------create list response-----------')
            console.log(response);
            this.setState({loading: false});
            if (response.status === 200) {
                console.log('create list sucesssss')
                this.getList()
            }
        } catch (error) {
            this.setState({loading: false});
            console.log(error)
        }
    }

// Redeem coupon Api
    addTolistOrRedeem = async (requestType) => {
        this.setState({loading: true});
        var _this = this
        let body = {
            "coupon_id": this.state.item.id,
            "list_id": this.state.item.list,
            "win_id": this.state.item.win_id,
            "request_type": requestType
        };
        try {
            let response = await api.request('add-to-list-or-redeem', 'post', body);
            console.log('-----------Redeem response-----------')
            console.log(response);
            console.log('-----------response-----------')
            this.setState({loading: false});
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        _this.setState({item: data.coupon[0]})
                        this.setState({isRedeem: true});
                    })
                } catch (error) {
                    console.log(error)
                }
                // this.WebViewRef && this.WebViewRef.reload();
                console.log('sucesssss')
            }
        } catch (error) {
            this.setState({loading: false});
            console.log(error)
        }
    }

    //Copy Paste Promo code option write from clipboard
    writeToClipboard = async () => {
        await Clipboard.setString(this.state.item.promo_code);
        Alert.alert
        (
            '',
            'your promo code ' + this.state.item.promo_code + ' is copied',
            [
                {text: 'OK', onPress: () => Linking.openURL(this.state.item.redemption_url)},
            ],
            {cancelable: false}
        )
    };

    actionRedeemButton() {
        this.setState({
            modalVisible: 'redeem',
        });
    }

    addTolist = async () => {
        this.setState({loading: true});
        let body = {
            "win_id": this.state.item.win_id,
            "list_ids": this.state.allSelectedItemList
        };
        console.log('---------', body)
        try {
            let response = await api.request('add-to-lists', 'post', body);
            console.log('-----------addTolist response-----------')
            console.log(response);
            this.setState({loading: false});
            if (response.status === 200) {
                console.log('sucesssss')
            }
        } catch (error) {
            this.setState({loading: false});
            console.log(error)
        }
    }

    renderViewMore(onPress) {
        return (
            <Text style={{color: '#3E86A3', marginLeft: 10}} onPress={onPress}>View more</Text>
        )
    }

    renderViewLess(onPress) {
        return (
            <Text style={{color: '#3E86A3', marginLeft: 10}} onPress={onPress}>View less</Text>
        )
    }

    render() {
        const iphoneHeight = Dimensions.get('window').height
        // var str = 'jhgjbvjbbbvjhbdfbvduuujfjvbadgjlhlgldhglidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivhjhgjbvjbbbvjhbdfbvduuujfjvbadgjlhlgldhglidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivhjhgjbvjbbbvjhbdfbvduuujfjvbadgjjgjkgjkghvjhlidhgpwibuwbvubuebguiewbvgebvpuebpveuwibvpiirwvrwubvhuwbhuivh'
        var countstr = this.state.item.detail.length
        var temp
        if (!this.state.isMoreView) {
            if (countstr > 400) {
                temp = 700
            } else if (countstr > 300) {
                temp = 650
            } else if (countstr > 200) {
                temp = 600
            } else if (countstr > 100) {
                temp = 550
            } else {
                temp = 500
            }
        }
        var viewHeight = (this.state.isMoreView) ? (iphoneHeight * 58) / 100 : temp
        return (
            <Background home={require('../Assets/home.png')} left={require('../Assets/back.png')}
                        leftNavigation={this.props.navigation}
                        navigation={this.props.screenProps.SignedOutNavigation}
            >

                <View style={{
                    width: '96%',
                    height: viewHeight,
                    margin: '2%',
                    shadowColor: 'rgba(75,76,77,0.75)',
                    backgroundColor: '#E5E5E5'
                }}>
                    <Text style={{color: '#3E86A3', fontSize: 20, fontWeight: '800', marginLeft: '3%'}}>DETAILS</Text>

                    {(this.state.item.redeemed === 0) ? (
                        <ImageBackground source={require('../Assets/playav-bg.png')}
                                         resizeMode='stretch'
                                         style={{
                                             flex: 1, margin: '2%'
                                         }}>

                            <View style={{width: '100%', flexDirection: 'row', height: '18%', marginTop: '1%'}}>
                                <Image style={{width: '20%', height: '90%', margin: '0.5%'}}
                                       source={{uri: this.state.item.logo, cache: 'force-cache'}}
                                       resizeMode='contain'/>
                                <Text style={{
                                    color: '#3E86A3',
                                    fontSize: 30,
                                    fontWeight: 'bold',
                                    textAlign: "right",
                                    width: '70%'
                                }} numberOfLines={2}>{this.state.item.offer}</Text>
                            </View>

                            <View style={{width: '100%', flexDirection: 'row', height: '15%', marginTop: '1%'}}>
                                <Text style={{
                                    color: '#3E86A3',
                                    fontSize: 16,
                                    fontWeight: '400',
                                    marginLeft: '3%',
                                    textAlign: 'center'
                                }}>{this.state.item.description}</Text>
                            </View>

                            <View style={{width: '100%', flexDirection: 'row', height: '40%', marginTop: '1%'}}>
                                <Image style={{width: '100%', height: '98%', margin: '1%'}}
                                       source={{uri: this.state.item.image, cache: 'force-cache'}}
                                       resizeMode='contain'/>
                            </View>

                            <ViewMoreText style={{width: '100%', flexDirection: 'row', height: '17%', marginTop: 5}}
                                          numberOfLines={2}
                                          renderViewMore={this.renderViewMore}
                                          renderViewLess={this.renderViewLess}
                                          afterExpand={() => {
                                              //alert('after expand')
                                              this.setState({isMoreView: false})
                                          }}
                                          afterCollapse={() => {
                                              // alert('after collapse')
                                              this.setState({isMoreView: true})
                                          }}
                                          textStyle={{textAlign: 'left', marginHorizontal: 10}}
                            >
                                <Text style={{
                                    color: '#000000',
                                    fontSize: 14,
                                    fontWeight: '300',
                                    marginLeft: '3%',
                                    textAlign: 'left'
                                }}>{this.state.item.detail}
                                </Text>
                            </ViewMoreText>
                            {/* <View style={{width:'100%',flexDirection:'row',height:'19%',marginTop:'1%'}}>
                <Text style={{color: '#3E86A3', fontSize: 16, fontWeight: '400',marginLeft:'3%',textAlign:'center'}} numberOfLines={2}>{str}</Text>
                </View> */}

                            <View style={{width: '100%', flexDirection: 'row', height: '5%'}}>
                                <Text style={{
                                    color: '#3E86A3',
                                    fontSize: 14,
                                    fontWeight: '400',
                                    textAlign: "right",
                                    width: '97%'
                                }} numberOfLines={1}>{this.state.item.end_date}</Text>
                            </View>

                        </ImageBackground>
                    ) : (

                        <ImageBackground source={require('../Assets/playav-bg.png')}
                                         resizeMode='stretch'
                                         style={{
                                             flex: 1, margin: '2%'
                                         }}>

                            <View style={{width: '100%', flexDirection: 'row', height: '15%', marginTop: '1%'}}>
                                <Image style={{width: '25%', height: '100%', margin: '1%'}}
                                       source={{uri: this.state.item.logo, cache: 'force-cache'}}
                                       resizeMode='contain'/>
                                <Text style={{
                                    color: '#3E86A3',
                                    fontSize: 30,
                                    fontWeight: 'bold',
                                    textAlign: "right",
                                    width: '70%'
                                }} numberOfLines={2}>{this.state.item.offer}</Text>
                            </View>

                            <View style={{width: '100%', flexDirection: 'row', height: '13%', marginTop: '1%'}}>
                                <Text style={{
                                    color: '#3E86A3',
                                    fontSize: 16,
                                    fontWeight: '400',
                                    marginLeft: '3%',
                                    textAlign: 'center'
                                }} numberOfLines={2}>{this.state.item.description}</Text>
                            </View>

                            <View style={{
                                width: '100%',
                                flexDirection: 'row',
                                height: '30%',
                                marginTop: '1%',
                                marginBottom: '1%'
                            }}>
                                <Image style={{width: '100%', height: '98%', margin: '1%'}}
                                       source={{
                                           uri: this.props.navigation.state.params.item.image,
                                           cache: 'force-cache'
                                       }}
                                       resizeMode='contain'/>
                            </View>

                            <ViewMoreText style={{width: '100%', flexDirection: 'row', height: '17%'}}
                                          numberOfLines={2}
                                          renderViewMore={this.renderViewMore}
                                          renderViewLess={this.renderViewLess}
                                          afterExpand={() => {
                                              //alert('after expand')
                                              this.setState({isMoreView: false})
                                          }}
                                          afterCollapse={() => {
                                              // alert('after collapse')
                                              this.setState({isMoreView: true})
                                          }}
                                          textStyle={{textAlign: 'left', marginHorizontal: 10}}
                            >
                                <Text style={{
                                    color: '#000000',
                                    fontSize: 14,
                                    fontWeight: '300',
                                    marginLeft: '3%',
                                    textAlign: 'left',
                                }}>{this.state.item.detail}
                                </Text>
                            </ViewMoreText>

                            {/* <View style={{width:'100%',flexDirection:'row',height:'17%',marginTop:'1%'}}>
                <Text style={{color: '#3E86A3', fontSize: 16, fontWeight: '400',marginLeft:'3%',textAlign:'center'}} numberOfLines={2}>{this.state.item.detail}</Text>
                </View> */}

                            <View style={{
                                width: '100%',
                                flexDirection: 'row',
                                height: '16%',
                                marginTop: '2%',
                                justifyContent: 'center'
                            }} onLayout={(event) => {
                                var {x, y, width, height} = event.nativeEvent.layout;
                                var barW = (width * 65) / 100
                                this.setState({barCodeHeight: height, barCodeWidth: barW})
                            }}>
                                {(this.state.item.code_type === 'upc') ? (<Barcode
                                    bgColor={'#FFFFFF'}
                                    fgColor={'#000000'}
                                    format={Formats.UPC_A}
                                    value={this.state.item.promo_code}
                                    width={this.state.barCodeWidth}
                                    height={this.state.barCodeHeight}
                                />) : (
                                    <View style={{
                                        width: '70%',
                                        flexDirection: 'column',
                                        height: this.state.barCodeHeight,
                                        marginTop: '2%',
                                        justifyContent: 'center'
                                    }}>
                                        <Text style={{
                                            color: '#3E86A3',
                                            fontSize: 20,
                                            fontWeight: '400',
                                            width: '97%',
                                            textAlign: 'center'
                                        }} numberOfLines={1}>{this.state.item.promo_code}</Text>
                                    </View>
                                )}

                                <View style={{
                                    width: '25%',
                                    flexDirection: 'column',
                                    height: '100%',
                                    marginTop: '2%',
                                    justifyContent: 'center'
                                }}>
                                    <Text style={{
                                        color: '#3E86A3',
                                        fontSize: 16,
                                        fontWeight: '400',
                                        width: '97%',
                                        textAlign: 'center'
                                    }} numberOfLines={1}>Expires</Text>
                                    <Text style={{
                                        color: '#3E86A3',
                                        fontSize: 14,
                                        fontWeight: '400',
                                        textAlign: "right",
                                        width: '97%'
                                    }} numberOfLines={2}>{this.state.item.end_date}</Text>

                                </View>
                            </View>


                        </ImageBackground>
                    )}


                </View>

                <View style={{flexDirection: 'row', width: '90%', justifyContent: 'space-between', marginLeft: '1%'}}>
                    <Button name={'EXCHANGE'} paddingHorizontal={10} fontSize={PixelRatio.get() * 6}
                            disabled={this.state.isRedeem}
                            onPress={() =>
                                this.actionRedeemButton()
                            }/>

                    <Button name={'ONLINE'} paddingHorizontal={10} fontSize={PixelRatio.get() * 6}
                            disabled={(this.state.item.online_redemption === 'no') ? true : (this.state.isReedemdPress === false ? true : false)}
                            onPress={() => this.writeToClipboard()}
                    />

                    <Button name={'ADD TO LIST'} paddingHorizontal={10} fontSize={PixelRatio.get() * 6}
                            disabled={this.state.isRedeem}
                            onPress={() => this.setState({modalVisible: 'addToList'})}/>

                </View>
                <Modal animationType="fade" transparent={true} visible={this.state.modalVisible ? true : false}>
                    <TouchableWithoutFeedback onPress={() => {
                        this.setState({modalVisible: false})
                    }}>
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'rgba(0,0,0,0.67)'
                        }}>
                            <TouchableWithoutFeedback>
                                {this.renderModalContent()}
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {this.state.loading ?
                    <Loader/> : null
                }
            </Background>
        )
    }

    getItemId = (selected_data) => {
        console.log(selected_data);
        let index = this.state.allSelectedItemList.indexOf(selected_data);
        console.log(index);
        if (index === -1) {
            this.setState({allSelectedItemList: [...this.state.allSelectedItemList, selected_data]})
        } else {
            let array = [...this.state.allSelectedItemList]; // make a separate copy of the array
            array.splice(index, 1);
            this.setState({allSelectedItemList: array});
        }
    }


    renderModalContent() {
        if (this.state.modalVisible === 'redeem') {
            return (
                <View style={{
                    backgroundColor: '#4191AA',
                    alignItems: 'center',
                    marginHorizontal: 30,
                    padding: 20,
                    borderRadius: 10
                }}>
                    <Image source={require('../Assets/caution.png')}/>
                    <Text style={{textAlign: 'center', marginTop: 10, color: '#FFFFFF', fontSize: 16}}>Are you sure?
                        Once exchanged, it can not be used again.</Text>
                    <View style={{flexDirection: 'row', marginTop: 15, width: '60%', justifyContent: 'space-around'}}>
                        <Button name={'YES'} paddingHorizontal={10} height={35} onPress={() => {
                            this.setState({
                                modalVisible: false,
                                isReedemdPress: true,
                            });
                            this.addTolistOrRedeem('redeem');
                            // navigate.navigateTo(this.props.navigation, 'RedeemedCoupon');
                        }}/>
                        <Button name={'NO'} shadowColor={'#C92828'} paddingHorizontal={10} height={35}
                                onPress={() => this.setState({modalVisible: false})}/>
                    </View>
                </View>
            )
        } else if (this.state.modalVisible === 'addToList') {
            return (
                <View style={{backgroundColor: '#E5E5E5', width: '90%'}}>
                    <KeyboardAwareScrollView behavior="position" enabled>
                        <Text style={{
                            alignSelf: 'center',
                            marginVertical: 15,
                            color: '#6D6D6D',
                            fontSize: 22,
                            fontWeight: '600'
                        }}>
                            Add to list
                        </Text>
                        <View style={{height: 220}}>
                            <FlatList
                                // data={['New year', 'Easter', 'Christmas', 'New year', 'Easter', 'Christmas']}
                                data={this.state.myList}
                                extraData={this.state.allSelectedItemList}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item}) =>
                                    <TouchableOpacity onPress={() => {

                                        this.getItemId(item.id)

                                    }}>
                                        <View style={styles.listView}>
                                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                                <Image source={require('../Assets/my-list.png')}/>
                                                <Text
                                                    style={{
                                                        marginLeft: 20,
                                                        fontWeight: 'bold',
                                                        letterSpacing: 2,
                                                        width: '65%'
                                                    }}
                                                    numberOfLines={1}>{item.name}</Text>
                                            </View>
                                            {
                                                (this.state.allSelectedItemList.indexOf(item.id) >= 0 ? (
                                                    <Image
                                                        source={require('../Assets/check-list.png')}/>
                                                ) : null)
                                            }

                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        <Text style={{alignSelf: 'center', fontSize: 20, fontWeight: 'bold'}}>or</Text>
                        <KeyboardAvoidingView behavior="padding" enabled keyboardVerticalOffset={100}>
                            <View style={{flexDirection: 'row', justifyContent: 'center', marginVertical: 20}}>
                                <TextInput placeholder={'Enter new list name'} placeholderTextColor={'#CBCBCB'}
                                           autoCorrect={false}
                                           onChangeText={(listName) => this.setState({listName})}
                                           value={this.state.listName}
                                           underlineColorAndroid={'transparent'}
                                           style={{
                                               backgroundColor: '#8E8C8C',
                                               height: 50,
                                               width: '65%',
                                               paddingLeft: 10,
                                               color: 'white'
                                           }}/>
                                <TouchableOpacity>
                                    <View style={{
                                        backgroundColor: '#4191AA', height: 50, alignItems: 'center',
                                        justifyContent: 'center', paddingHorizontal: 15
                                    }}>
                                        <Text style={{color: 'white', fontWeight: '800'}} onPress={() => {
                                            this.createList();
                                            Keyboard.dismiss()
                                        }
                                        }>Create</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAvoidingView>
                        <View>
                            <Button name={'SAVE'} paddingHorizontal={10} fontSize={PixelRatio.get() * 8}
                                    onPress={() => {
                                        this.addTolist();
                                        this.setState({modalVisible: false})
                                    }}/>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
            )
        }
    }
};

const styles = StyleSheet.create({
    listView: {
        flexDirection: 'row',
        borderTopColor: '#A3A2A2',
        borderTopWidth: 1,
        height: 70,
        alignItems: 'center',
        paddingHorizontal: 20,
        justifyContent: 'space-between'
    }
});
