import React, { Component, PureComponent } from 'react';
import {
    Text,
    View,
    Image,
    ImageBackground,
    FlatList,
    TouchableOpacity,
    AsyncStorage,
    AppState,
    StatusBar,
    Modal,
    TouchableWithoutFeedback,
    StyleSheet,
    Animated
} from 'react-native';
import navigate from '../Components/navigate';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import firebase from 'react-native-firebase';
import SlotMachine from 'react-native-slot-machine';
import SoundPlayer from 'react-native-sound-player';
import ViewMoreText from 'react-native-view-more-text';
import Dimensions from 'Dimensions';
import api from '../api';

const _ = require('underscore');
const color = ['#336979', '#8DA9B7'];
var blinkingTime, winBlinkTime;

class SlotImageComponent extends PureComponent {

    render() {       
        return (
            <View style={{ width: this.props.width,height:this.props.height, alignItems: 'center', justifyContent: 'center' }}>
                <Image resizeMode='contain' source={require('../Assets/slot-box.png')} style={{position:'absolute',height:'100%',width:'100%' }}/>
          
                <FastImage
                    style={{ width:this.props.width - 10, height: this.props.height }}
                    source={ this.props.slotImage === "" ? 
                    ( 
                        (Math.floor(Math.random() * Math.floor(4))) == 0 ? require('../Assets/9090909090.png') :
                            ( (Math.floor(Math.random() * Math.floor(4))) == 1 ? require('../Assets/9191919191.png') :
                                (
                                    (Math.floor(Math.random() * Math.floor(4))) == 2 ? require('../Assets/9292929292.png') :
                                    require('../Assets/9393939393.png')
                                )
                           )
                    ) : {
                        uri:  this.props.slotImage,
                        headers:{ Authorization: 'someAuthToken' },
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
               {this.props.checkRowColumn?this.props.horizontal!==null && this.props.horizontal === true?<View style={{position:'absolute', top:(this.props.height)/2, left:0, width:'100%',height:4.5, backgroundColor:'#FEDD7A'}}></View>
                   : <View style={{position:'absolute', top:0, left:(this.props.width)/2, width:4.5, height:'100%', backgroundColor:'#FEDD7A'}}></View>
                   :null}
            </View>
        );
    }
}


class SlotMachineComponent extends PureComponent {
    render(){
        return (
            <View style={{ width: 90, height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                
                <FastImage
                    style={{ paddingHorizontal: 10, marginVertical: 10, width: 55, height: '30%' }}
                    source={ this.props.imageSlot === "" ? require('../Assets/9292929292.png' ) : {
                            uri:  this.props.imageSlot,
                            headers:{ Authorization: 'someAuthToken' },
                            priority: FastImage.priority.normal,
                        }}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <FastImage
                    style={{ paddingHorizontal: 10, marginVertical: 10, width: 55, height: '30%' }}
                    source={ this.props.imageSlot === "" ? require('../Assets/9090909090.png') : {
                        uri:  this.props.imageSlot,
                        headers:{ Authorization: 'someAuthToken' },
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <FastImage
                     style={{ paddingHorizontal: 10, marginVertical: 10, width: 55, height: '30%' }}
                    source={ this.props.imageSlot === "" ? require('../Assets/9191919191.png') : {
                        uri:  this.props.imageSlot,
                        headers:{ Authorization: 'someAuthToken' },
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
            
            </View>
        );
    }
}


export default class Games extends Component {
    imageArray = [];
    adsImage = [];
    tempData = '';

    constructor(props) {
        super(props);
        this.state = { appState: AppState.currentState,
            isGameScreen:true,
            duration: 5000,
             slot1: '4444',
             loading: false,
             showAds: false,
            //  adsImage: [],
             imageBlinking: false,
             winImageBlink: false,
             url: '', url1: '', url2: '',
             isWon:false,
             winResObj:'',
             spinDisable:false,
             isMusic:'1',
             flatListheight:0,
             flatListWidth:0,
             modalVisible: false,
             isEntry:true,
             fromCoupon: this.props.navigation.state.params.fromCoupon,
             gameTypeKey: (this.props.navigation.state.params) ? this.props.navigation.state.params.gameKey :'auto,baby,books,clothing,flowers,food,jewelry,pets,restaurants,sporting,home,personal,travel,electronics,services,toys',
             gameTypeSepratedArr: (this.props.navigation.state.params) ? this.props.navigation.state.params.gameKey : [],
             subCategoryKeyCount: this.props.navigation.state.params.subCategoryCount,
             staticImgArr:['9090909090.png','9191919191.png','9292929292.png','9393939393.png'],
             rowOrColumn:'',
             horizontal: null,
             animatedValue: new Animated.Value(0)
            };
            firebase.analytics().setCurrentScreen('Game');

    };
    
    async componentDidMount() { 
        // Start Loader
         this.setState({loading:true})
        //  console.log('DidMount')
        //  console.log('GameTypeKey' + this.state.gameTypeKey)
        //  console.log('gameTypeSepratedArr' + this.state.gameTypeSepratedArr)
        //  console.log('subCategoryKeyCount' + this.state.subCategoryKeyCount)

        // Get 3 Images for slot machine
        for (let i = 0; i < 3; i++) {
           //If subcategory count less than 3 then add Temporary dictionary 
           if ( (this.state.subCategoryKeyCount < 3) && ((i + this.state.subCategoryKeyCount) < 3) ) {
            // console.log('createTempDictCountLessThanTwoCase')
                 this.createTempDictCountLessThanTwoCase(i)
           }else{
               // Get Images from AdButtler
            // console.log('getImageUrl1')
               this.getImageUrl(i);
           }
        }
        // Get 12 Images for match
        this.createArray();

         SoundPlayer.onFinishedPlaying( async (success) => { 
            var status = await AsyncStorage.getItem('isMusic');
            if (!this.state.isWon && this.state.isGameScreen){
                if(status === '1'){
                    SoundPlayer.playSoundFile('ambient_background_noise', 'wav')
                }
            }
        });

    Animated.timing(this.state.animatedValue,{
        toValue:1,
        duration:20000,
        useNativeDriver: true,
    }).start()
    }
       
    
// Clear all subscription 
        componentWillUnmount() {
            SoundPlayer.unmount();
            clearInterval(winBlinkTime);
            clearInterval(blinkingTime);
            AppState.removeEventListener('change', this._handleAppStateChange);
          }


     getValidKeyWhenCountIsZero(){
        //first check all count data then go for subcategory data
        if (this.state.subCategoryKeyCount > 0) {
          //if count less than 2 add extra temp dict
          var gameTypeKeyStr;
            do {
                var index = (Math.floor(Math.random() * Math.floor(this.state.gameTypeSepratedArr.length)))
                gameTypeKeyStr = this.state.gameTypeSepratedArr[index]
                console.log('GameTypeKeyStr' + JSON.stringify(gameTypeKeyStr))
            } while (gameTypeKeyStr.count == 0);
            console.log('gameTypeKeyStr' + gameTypeKeyStr.key)
            return gameTypeKeyStr.key
         }
      }  
      
      //alt_text key fill randomly from the static image array
      createTempDictCountLessThanTwoCase(arrCounter){
        var index = (Math.floor(Math.random() * Math.floor(3)))
        var localDict = {
            banner_id: "9191919191",
            redirect_url: "",
            image_url: "",
            width: "200",
            height: "200",
            alt_text:  this.state.staticImgArr[index], 
            accompanied_html: "",
            target: "_top",
            tracking_pixel: "",
            refresh_url: "",
            refresh_time: "",
            body: ""
         }
         this.renderMachineComponent(localDict, arrCounter);
            if (arrCounter == 1) {
                this.setState({ url: "" });
            }
            if (arrCounter == 2) {
                this.setState({ url1: "" });
            }
            if (arrCounter == 0) {
                 this.setState({ url2: "" });
            }
        
      }


      createTempDict_CountLessThanTwoCase(arrCounter){
        let _this = this;
        var index = (Math.floor(Math.random() * Math.floor(3)))
        var localDict = {
            banner_id: "9191919191",
            redirect_url: "",
            image_url: "",
            width: "200",
            height: "200",
            alt_text:  this.state.staticImgArr[index], 
            accompanied_html: "",
            target: "_top",
            tracking_pixel: "",
            refresh_url: "",
            refresh_time: "",
            body: ""
         }
         console.log('Local Data' + arrCounter)
         _this.renderMachineComponent( localDict, arrCounter);

                // setTimeout(function () {
                //     _this.renderMachineComponent( localDict, arrCounter);
                // }, 1000);
       }


    async getImageUrl(arrCounter) {
      
        let _this = this;
        
        try {
            var urlString = 'http://ab173053.adbutler-chargino.com/adserve/;ID=173053;size=51x51;setID=318156;kw=' + this.getValidKeyWhenCountIsZero() + ';type=json'
           
            let response = await api.getRequest(urlString, 'get');
            if (response.status === 200) {
               
                try {

                    await response.json().then((data) => {

                        if (data.status === "NO_ADS"){
                            _this.createTempDictCountLessThanTwoCase(arrCounter);
                        }else{
                            this.tempData = data.placements.placement_1;
                        _this.renderMachineComponent(data.placements.placement_1, arrCounter);
                        if (arrCounter == 1) {
                            this.setState({ url: data.placements.placement_1.image_url + "&banner_id=" + data.placements.placement_1.banner_id });
                        }
                        if (arrCounter == 2) {
                            this.setState({ url1: data.placements.placement_1.image_url + "&banner_id=" + data.placements.placement_1.banner_id });
                        }
                        if (arrCounter == 0) {
                             this.setState({ url2: data.placements.placement_1.image_url + "&banner_id=" + data.placements.placement_1.banner_id });
                        }
                    }
                    });

                } catch (error) {
                    if(this.tempData === ''){
                    _this.createTempDictCountLessThanTwoCase(arrCounter);
                    }else{
                        _this.renderMachineComponent(this.tempData, arrCounter);
                    }
                    // this.setState({ loading: false });
                    // console.log('Loding False')
                }
            } else {
                if(this.tempData === ''){
                    _this.createTempDictCountLessThanTwoCase(arrCounter);
                    }else{
                        _this.renderMachineComponent(this.tempData, arrCounter);
                    }            
                    // this.setState({ loading: false });
                // await response.json().then((res) => {
                //     this.setState({ loading: false });
                //     console.log('Loding False')

                // }
                // );
            }
        } catch (error) {
        }
    }


    async  getAds(arrCounter) {
      
        let _this = this;
        // this.setState({loading: true});
        try {
            var urlString = 'http://ab173053.adbutler-chargino.com/adserve/;ID=173053;size=51x51;setID=318156;kw=' + this.getValidKeyWhenCountIsZero() + ';type=json'

            let response = await api.getRequest(urlString, 'get');
            console.log('Response' + arrCounter + '  ' + response.length)
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                    // check for no add
                        if (data.status === "NO_ADS"){
                            _this.createTempDict_CountLessThanTwoCase(arrCounter)
                        }else{
                            this.tempData = data.placements.placement_1;
                            _this.renderMachineComponent(data.placements.placement_1, arrCounter);
                        // setTimeout(function () {
                        //     _this.renderMachineComponent(data.placements.placement_1, arrCounter);
                        // }, 1000);
                        }
                    });

                } catch (error) {
                    // console.log('error' + error + response.status)
                    // console.log(urlString)
                    // console.log(JSON.stringify(response))
                    if(this.tempData === ''){
                    _this.createTempDict_CountLessThanTwoCase(arrCounter)
                    }else{
                        _this.renderMachineComponent(this.tempData, arrCounter);
                    }
                }
            } else {
                // console.log('wrong ' + arrCounter + response.status)
                // console.log(urlString)
                // console.log(JSON.stringify(response))
                if(this.tempData === ''){
                    _this.createTempDict_CountLessThanTwoCase(arrCounter)
                    }else{
                        _this.renderMachineComponent(this.tempData, arrCounter);
                    }
            }
        } catch (error) {
            // console.log('try error' + error)
            // console.log(urlString)
            // console.log(JSON.stringify(response))
            if(this.tempData === ''){
                _this.createTempDict_CountLessThanTwoCase(arrCounter)
                }else{
                    _this.renderMachineComponent(this.tempData, arrCounter);
                }
        }
    }

    

   
      _handleAppStateChange = async (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
          console.log('App has come to the foreground!')
          if(this.getMusicStatus() === '1'){
          SoundPlayer.resume()
          console.log('resume music in game')
          }else{
            SoundPlayer.pause()
            console.log('pause music in game')
          }
        }else{
            SoundPlayer.pause()
            console.log('pause music in game')
        }
        this.setState({appState: nextAppState});
      }


   async startSpinning() {
        try {
            var status = await AsyncStorage.getItem('isMusic');
        if(status === '1'){
            SoundPlayer.playSoundFile('slot_wheel_spin', 'mp3')
            }
        } catch (e) {
            console.log(e)
        }       
        this.setState({ spinDisable: true, isWon: false }); //Disable the play button touch
        let val1 = Math.floor(Math.random() * 9000) + 1000;

        this.setState({ showAds: false, isEntry: false, duration: 5000, slot1: val1});

        setTimeout (() => {
            
            this.setState({ showAds: true, spinDisable: false })
            if(!this.state.isEntry && !this.state.isWon){
                this.calculateResult()
               }

        }, 4000) // stop spining view in 4 sec

        this.imageArray = [];
        this.createArray();
    }



    renderMachineComponent(data, arrCounter) {
        // console.log('when add data in ads array ==>'+ JSON.stringify(data) + ' counter == '+arrCounter)
        this.imageArray[arrCounter] = data;
        // console.log('Data Count' + JSON.stringify(data) + '   ' + arrCounter)
        if (_.compact(this.imageArray).length === 12) 
        {
            this.adsImage =  _.compact(this.imageArray);

            //  this.setState({ adsImage: _.compact(this.imageArray) });
            
           if(!this.state.isEntry && !this.state.isWon){
            // this.calculateResult()
           }else{
                this.setState({ showAds: true })
              this.startMusic();
           }

    

        setTimeout( ()=> {
            this.setState({
                loading: false,
               })
               console.log('Loding False')

        }, 1000);
           
        }
    };
 // First Entry time

    async startMusic(){
                var status = await AsyncStorage.getItem('isMusic');
                    this.setState({isMusic: status});
                
                AppState.addEventListener('change', this._handleAppStateChange);
 
                try {
                    if (status === '1'){
                    SoundPlayer.playSoundFile('entering_slot_machine_game', 'wav')
                }
            } catch (e) {
                console.log(e)
            }
        }

        
          


    createArray() {
        console.log('createArray')
        randomRange = length => {
            const results = []
            const possibleValues = Array.from({ length }, (value, i) => i)
            console.log('createArray possible Values' + possibleValues)

            for (let i = 0; i < length; i += 1) {
              const possibleValuesRange = length - (length - possibleValues.length)
              const randomNumber = Math.floor(Math.random() * possibleValuesRange)
              const normalizedRandomNumber = randomNumber !== possibleValuesRange ? randomNumber : possibleValuesRange
              const [nextNumber] = possibleValues.splice(normalizedRandomNumber, 1)
              results.push(nextNumber)
            }
          console.log('Create array Result' + results)
            return results
          }
       

        if(this.state.subCategoryKeyCount > 0)  //if no count exist then all data are from temp dict
        {
            var tempDictRun = 0;
            if(this.state.subCategoryKeyCount === 1){
                tempDictRun = 8
            }else if(this.state.subCategoryKeyCount === 2){
                tempDictRun = 4
            }

            // var tempDictRun = (3 - this.state.subCategoryKeyCount) * 4 ;
            var randomPosArr = randomRange(12); 
            // console.log('460 randomPosArr' + randomPosArr)

             randomPosArr =  randomPosArr.slice(0, tempDictRun);
             console.log('after slice' + randomPosArr.length);

            for (let i = 0; i < 12; i++) {
              //Temp dict and original api call according to subCategoryKeyCount ratio and also in random postion

                if ( (this.state.subCategoryKeyCount < 3) && ((i + this.state.subCategoryKeyCount) < 12) ) {
                    console.log('467')
                    if(randomPosArr.indexOf(i) > -1) {
                        this.createTempDict_CountLessThanTwoCase(i)
                    }else{
                        console.log('inner index ' + i)

                        this.getAds(i);
                    }
                  }
                else{
                    console.log('outer index ' + i)

                    this.getAds(i);
                    
                }
            }
         }
         else{
            for (let i = 0; i <= 12; i++) {
               this.createTempDict_CountLessThanTwoCase(i)
            }
         }
     }
     

calculateResult(){
   console.log('--------------- calculate result -----------');
       if (this.compareItems([this.adsImage[4].alt_text,this.adsImage[5].alt_text,this.adsImage[6].alt_text,this.adsImage[7].alt_text])){
           this.result(this.adsImage[4].banner_id);
           this.setState({rowOrColumn:[4,5,6,7],horizontal:true});
       }else if (this.compareItems([this.adsImage[0].alt_text,this.adsImage[1].alt_text,this.adsImage[2].alt_text,this.adsImage[3].alt_text])){
           this.result(this.adsImage[0].banner_id);
           this.setState({rowOrColumn:[0,1,2,3],horizontal:true});
       }else if (this.compareItems([this.adsImage[8].alt_text,this.adsImage[9].alt_text,this.adsImage[10].alt_text,this.adsImage[11].alt_text])){
           this.result(this.adsImage[8].banner_id);
           this.setState({rowOrColumn:[8,9,10,11],horizontal:true});
       }else if (this.compareItems([this.adsImage[0].alt_text,this.adsImage[4].alt_text,this.adsImage[8].alt_text])){
           this.result(this.adsImage[0].banner_id);
           this.setState({rowOrColumn:[0,4,8],horizontal:false});
       }else if (this.compareItems([this.adsImage[1].alt_text,this.adsImage[5].alt_text,this.adsImage[9].alt_text])){
           this.result(this.adsImage[1].banner_id);
           this.setState({rowOrColumn:[1,5,9],horizontal:false});
       }else if (this.compareItems([this.adsImage[2].alt_text,this.adsImage[6].alt_text,this.adsImage[10].alt_text])){
           this.result(this.adsImage[2].banner_id);
           this.setState({rowOrColumn:[2,6,10],horizontal:false});
       }else if (this.compareItems([this.adsImage[3].alt_text,this.adsImage[7].alt_text,this.adsImage[11].alt_text])){
           this.result(this.adsImage[3].banner_id);
           this.setState({rowOrColumn:[3,7,11],horizontal:false});
       }
       


       else if (this.compareItems([this.adsImage[4].alt_text,this.adsImage[5].alt_text,this.adsImage[6].alt_text])){
        this.result(this.adsImage[4].banner_id);
        this.setState({rowOrColumn:[4,5,6],horizontal:true});
       }else if (this.compareItems([this.adsImage[5].alt_text,this.adsImage[6].alt_text,this.adsImage[7].alt_text])){
        this.result(this.adsImage[5].banner_id);
        this.setState({rowOrColumn:[5,6,7],horizontal:true});
      } else if (this.compareItems([this.adsImage[0].alt_text,this.adsImage[1].alt_text,this.adsImage[2].alt_text])){
        this.result(this.adsImage[0].banner_id);
        this.setState({rowOrColumn:[0,1,2],horizontal:true});
       }else if (this.compareItems([this.adsImage[1].alt_text,this.adsImage[2].alt_text,this.adsImage[3].alt_text])){
        this.result(this.adsImage[1].banner_id);
        this.setState({rowOrColumn:[1,2,3],horizontal:true});
       }else if (this.compareItems([this.adsImage[8].alt_text,this.adsImage[9].alt_text,this.adsImage[10].alt_text])){
        this.result(this.adsImage[8].banner_id);
        this.setState({rowOrColumn:[8,9,10],horizontal:true});
       }else if (this.compareItems([this.adsImage[9].alt_text,this.adsImage[10].alt_text,this.adsImage[11].alt_text])){
        this.result(this.adsImage[9].banner_id);
        this.setState({rowOrColumn:[9,10,11],horizontal:true});
       }else{
           this.setState({rowOrColumn:[],horizontal:null})
       }
    
}

compareItems =  (objects) =>{
   console.log('compare result  ==> '+objects) 
   
    for (let i = 0; i < objects.length; i++) {
        if (objects[0] === objects[i]) {
             if ( (objects[i] != '9090909090.png') && (objects[i] != '9191919191.png') && (objects[i] != '9292929292.png') && (objects[i] != '9393939393.png'))
             {
                //console.log('compare within loop ==> '+objects[i])
                continue
              } else {
                return false
              }
        } else {
              return false            
            }
    }
  return true
}

result = async (bannerId) =>{
    var _this = this
    console.log('Call win animation method')
    this.animation();
       try {
            _this.state.isWon = true
           _this.setState({spinDisable:true}) // Disable Play Button
           var status = await AsyncStorage.getItem('isMusic');
            if(status === '1'){
               SoundPlayer.playSoundFile('coupon_win', 'wav')
            }
       } catch (e) {
           console.log(e)
       }
        this.winUpdateOnServer(bannerId)
}


winnerAnimate=()=>{
    winBlinkTime= setInterval(() => {
            this.setState({winImageBlink:!this.state.winImageBlink})
            
       }, 200);}

animation=()=>{
    blinkingTime= setInterval(() => {
            this.setState({imageBlinking: !this.state.imageBlinking})
        
   }, 200);}


winUpdateOnServer = async (bannerId) =>{
     var _this = this
    // var bannerId = this.state.url.split('=')
    let body = {
         "banner_id": bannerId,
     };
     console.log(body)
    try {
        let response = await api.request('win', 'post', body);
        console.log('response == '+response)

        if (response.status === 200) {
            try {
                await response.json().then((data) => {
                /////
                _this.setState({
                    winResObj:data,
                })
            
               
                setTimeout(function(){
                  _this.setState({
                     modalVisible: true,
                     spinDisable: false
                 })
                
                }, 150);
        
                    // setTimeout(function(){
                    //     // _this.reSetAllData();
                    //     _this.setState({ pressPlayGameButton: false }); //now play game touch button is enable.
                            
                    // }, 2000);
                
                })
                this.setState({
                    winImageBlink:false
                  });
                clearInterval(winBlinkTime);
                this.winnerAnimate();
                
            } catch (error) {
                this.setState({
                    winImageBlink:false,
                    spinDisable: false
                  });
                
                clearInterval(winBlinkTime);
                
                console.log(error)
            }
            console.log('sucesssss')
        }
    } catch (error) {
        this.setState({
            winImageBlink:false,
            spinDisable: false
          });
        
        clearInterval(winBlinkTime);
        console.log(error)
    }
}

keepCoupon = async () =>{
   // this.reSetAllData();
    clearInterval(blinkingTime);
    this.setState({
          modalVisible: false,
          imageBlinking:false
        })
    
}

discardCoupon = async () =>{
    var _this = this
    this.setState({modalVisible: false})
    var temp = 'discard/' + this.state.winResObj.win_id
    // _this.reSetAllData();
    try {
        let response = await api.request(temp, 'post');
        if (response.status === 200) {
            _this.setState({winResObj:''})
           
            ////
            clearInterval(winBlinkTime);
            clearInterval(blinkingTime);
            this.setState({
                imageBlinking:false,
                winImageBlink:false
              });
            
        
            console.log(' discardCoupon sucesssss')
        }else{
            console.log(' discardCoupon faile')
        }
    } catch (error) {
        console.log(error)
        console.log(' discardCoupon faile')
    }

}



reSetAllData = () =>{
    this.setState({
        duration: 5000,
        slot1: '4444',
        spinDisable:false
    })

    for (let i = 0; i < 3; i++) {
        //If subcategory count less than 3 then add Temporary dictionary 
        if ( (this.state.subCategoryKeyCount < 3) && ((i + this.state.subCategoryKeyCount) < 3) ) {
            this.createTempDictCountLessThanTwoCase(i)
        }else{
          this.getImageUrl(i);
       }
    }
    this.createArray();
}


async getMusicStatus (){
 var musicStatus = await AsyncStorage.getItem('isMusic')
 this.setState({isMusic: musicStatus})
}

setMusicState = async(music) =>{
    AsyncStorage.setItem('isMusic',music)
     if(music === '1'){
        try {
            const info = await SoundPlayer.getInfo() // Also, you need to await this because it is async
            SoundPlayer.resume()
        } catch (e) {
            console.log('There is no song playing', e)
            SoundPlayer.playSoundFile('ambient_background_noise', 'wav')
          }

     }else{
         SoundPlayer.pause()
     }
     this.getMusicStatus()
}

goHome = async () =>{
    SoundPlayer.pause()
    navigate.navigateWithReset(this.props.navigation, 'Dashboard')
}

renderViewMore(onPress){
    return(
      <Text style={{color:'#3E86A3',marginLeft:10}} onPress={onPress}>View more</Text>
    )
  }
  renderViewLess(onPress){
    return(
      <Text style={{color:'#3E86A3',marginLeft:10}} onPress={onPress}>View less</Text>
    )
  }

renderModalContent() {
    if (this.state.winResObj !== ''){
        return (
            <View style={{width:'65%',height:'35%'}}>

            <Image resizeMode='stretch' source={require('../Assets/playav-bg.png')} style={{position:'absolute',height:'100%',width:'100%' }}/>
            
            <View style={{ justifyContent:'center', alignItems:'center', flex:1}}>

                <Image resizeMode='stretch' source={require('../Assets/You_Won.png')} />
                
            <View style={{ height: 50}}/>

            <TouchableOpacity style={{width:100, height: 40, backgroundColor:'rgb(93,93,93)', justifyContent:'center'}} onPress={() =>this.keepCoupon()}>
                    <Text style={{ textAlign:'center', color:'white', fontSize:20}}>OK</Text>
                    </TouchableOpacity>
            </View>
            


            {/* Coupon details */}
            {/* <View style={{width:'100%',height:'90%'}}>

                <View style={{width:'100%',flexDirection:'row',height:'15%',marginTop:'1%'}}>

                <Image style={{  width: '25%', height: '100%',margin:'1%' }}
                               //source={require('../Assets/mcdonalds-logo.png')}
                                  source={{ uri: this.state.winResObj.coupon.logo ,cache: 'force-cache'}}
                                resizeMode='contain'/>
                <Text style={{color: '#3E86A3', fontSize: 30, fontWeight: 'bold',textAlign:"right",width:'70%'}} numberOfLines={2}>{this.state.winResObj.offer}</Text>         
                </View>

                <View style={{width:'100%',flexDirection:'row',height:'15%',marginTop:'1%'}}>
                <Text style={{color: '#3E86A3', fontSize: 16, fontWeight: '400',marginLeft:'3%',textAlign:'left'}} numberOfLines={3}>{this.state.winResObj.coupon.description}</Text>
                </View>

                <View style={{width:'100%',flexDirection:'row',height:'30%'}}>
                <Image style={{  width: '100%', height: '100%' }} 
                                //source={require('../Assets/banner.png')}
                                 source={{ uri: this.state.winResObj.coupon.image ,cache: 'force-cache'}}
                                resizeMode='contain'/>
                </View>
     
            <View style={{width:'100%',flexDirection:'row',height:'30%',marginTop:5}}>
               <ViewMoreText  numberOfLines={3}
                            renderViewMore={this.renderViewMore}
                             renderViewLess={this.renderViewLess}
                             textStyle={{textAlign: 'left',marginHorizontal:10}}>
               <Text style={{color: '#000000', fontSize: 14, fontWeight: '300',marginLeft:'3%',textAlign:'left'}}>{this.state.winResObj.coupon.detail}</Text>
           </ViewMoreText>
       </View>
   </View> */}


   
            {/* <View style={{height:'10%',width:'100%',flexDirection:'row',justifyContent:'space-between'}}>
            
                    <TouchableOpacity style={{width:'40%',left:'15%'}} onPress={() =>this.keepCoupon()}>
                    <Image resizeMode="contain" source={require('../Assets/keep.png')} style={{height:'100%',width:'100%'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'40%',right:'15%'}} onPress={() => this.discardCoupon()}>
                    <Image resizeMode="contain" source={require('../Assets/discard.png')} style={{height:'100%',width:'100%'}}/>
                    </TouchableOpacity>
            </View> */}
            </View>

        )
    }else{
        if (this.state.modalVisible){
            // console.log('No data in coupon response')
            clearInterval(blinkingTime);
            this.setState({
                  modalVisible: false,
                  imageBlinking:false
                })
            
        }
        return null
    }
}

checkRowColumn=(n1,n2)=>{
    if(n1.indexOf(n2) !== -1){
        return true
    }
      else return false;
    };

render() {
        
       var CompArray = new Array();
       CompArray[0] = <SlotMachineComponent imageSlot={this.state.url}  />;
       CompArray[1] = <SlotMachineComponent imageSlot={this.state.url1} />;
       CompArray[2] = <SlotMachineComponent imageSlot={this.state.url2} />;
       CompArray[3] = <SlotMachineComponent imageSlot={this.state.url} />;
       CompArray[4] = <SlotMachineComponent imageSlot={this.state.url1} />;
       CompArray[5] = <SlotMachineComponent imageSlot={this.state.url2} />;
       CompArray[6] = <SlotMachineComponent imageSlot={this.state.url} />;
       CompArray[7] = <SlotMachineComponent imageSlot={this.state.url1} />;
       CompArray[8] = <SlotMachineComponent imageSlot={this.state.url2} />;
       CompArray[9] = <SlotMachineComponent imageSlot={this.state.url} />;

const interpolatRotation = this.state.animatedValue.interpolate({
    inputRange:[0,1],
    outputRange:['0deg','5760deg'],
})
const animatedStyle =  {
    transform:[
        { rotate:interpolatRotation}
    ]
}

    return (
    
        <View style={{flex:1}}>
        <StatusBar hidden={true}/>
    
        <View style={{flex:2}}>
           <ImageBackground resizeMode="cover" source={require('../Assets/slot-head-bg.png')} style={{ width:'100%',height:'100%'}}>

           { (this.state.winResObj !== '') ?(
                 <View style={{height:'100%',width:'100%'}}>
                 
                    <View style={{flexDirection:'row',height:'50%',width:'100%'}}>

                        <Image resizeMode="contain" source={{ uri: this.state.winResObj.coupon.logo,cache: 'force-cache' }} style={{height:'60%',width:'30%',top:'8%'}}/>
                        <Text style={{color: '#3E86A3', fontSize: 30, fontWeight: 'bold',textAlign:"right",width:'60%',top:'5%'}} numberOfLines={2}>{this.state.winResObj.offer}</Text>         

                    </View>
                    <View style={{margin:'4%'}}>
                       <Text style={{color:'#3E86A3',fontSize: 16}} numberOfLines={2}>{this.state.winResObj.coupon.description}</Text>
                    </View>
                 
                 </View>)
                 : (null)
    }
                 <Image resizeMode='contain' source={require('../Assets/slot-head-bg-trans.png')} style={{position:'absolute',height:'100%',width:'100%' }}/>
            
                  <Image resizeMode='contain' source={require('../Assets/border.png')} style={
                      {
                          position:'absolute',height:'100%',width:'100%',
                          opacity: this.state.winImageBlink ?   0.4  : 1.0 
                       }
                      
                      }/>
                 
                 <TouchableOpacity style={{position:'absolute',height:'30%',width:'20%',right:'1%',top:'30%'}} onPress={this.goHome.bind(this)}>
                 <Image resizeMode='contain' source={require('../Assets/HomeIcon.png')} style={{height:'80%',width:'80%'}}/>
                </TouchableOpacity>
            </ImageBackground>
        </View>
    
        <View style={{flex:8}}>
             <ImageBackground
                  resizeMode="cover"
                 source={require('../Assets/full-bg.png')}
                 style={{ width:'100%',height:'100%'}}>
                    <View style={{flex:8}}>
                        <Image
                            resizeMode='contain'
                            source={require('../Assets/frame_1.png')}
                            style={{
                                   position:'absolute', top: '5%',left:'1%',height:'95%',width:'98%', 
                                   opacity: this.state.imageBlinking ?   0.4  : 1.0 
                        }}/>
                        
                        <Image
                         resizeMode='contain'
                         source={require('../Assets/inside.png')}
                         style={{ position:'absolute', top: '8%',left:'4%',height:'88%',width:'92%' }}
                        /> 

                       {this.state.showAds ?
                    <View style={{top: '10%', width: '92%',height:'85%',marginLeft:'4%'}} onLayout={(event) => {
                        var {x, y, width, height} = event.nativeEvent.layout;
                         this.setState({flatListheight:height,flatListWidth:width})
                        
                        }} > 
                       <FlatList
                        style={{}}
                        scrollEnabled={false}
                        numColumns={4}
                        data={this.adsImage}
                        extraData={this.state.showAds}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item,index }) =>{
                            {var linethru=this.checkRowColumn(this.state.rowOrColumn,index)};
                       return  <SlotImageComponent slotImage={item.image_url}  checkRowColumn={linethru} horizontal={this.state.horizontal} height={this.state.flatListheight / 3} width={(this.state.flatListWidth / 4)} />
                        }}
                    /> 
                    </View>
                    :
                    <View style={{top: '10%', width: '92%',marginLeft:'4%',height:'85%', backgroundColor: '#DFB872'}} onLayout={(event) => {
                        var {x, y, width, height} = event.nativeEvent.layout;
                         this.setState({flatListheight:height,flatListWidth:width})
                        
                        }}>

                        <SlotMachine
                            height={this.state.flatListheight}
                            width={'23%'}
                            text={this.state.slot1}
                            renderContent={c => <View style={{ backgroundColor: '#D3D6BF' }}>{CompArray[c]}</View>}
                            duration={this.state.duration}
                            range="0123456789"
                            useNativeDriver={true}
                             />
                             
                    </View>
                }

                </View>
                 <View style={{flex:2,flexDirection:'row',justifyContent:'space-between'}}>

                    <TouchableOpacity style={{width:'30%',flexDirection:'column',justifyContent:'center'}}onPress={() => { SoundPlayer.pause(),this.props.navigation.goBack()}} >
                    <Image resizeMode="contain" source={require('../Assets/catogery.png')} style={{height:'50%',width:'100%'}}/>
                    <Text style={{alignSelf:'center',color:'white',textAlign:'center'}}>{(this.state.fromCoupon) ? 'Select coupon' : 'Select Category'}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.startSpinning.bind(this)} style={{width:'30%'}}
                       disabled={this.state.spinDisable}
                      >
                     <Image resizeMode="contain" source={require('../Assets/plasav-btn.png')} style={{height:'100%',width:'100%'}}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{width:'30%',flexDirection:'column',justifyContent:'center'}} onPress={() => this.setMusicState((this.state.isMusic === '1') ? '0' : '1')}>
                    <Image resizeMode="contain" source={(this.state.isMusic === '1') ? require('../Assets/unmute.png') : require('../Assets/mute.png')} style={{height:'50%',width:'100%'}}/>
                    </TouchableOpacity>

                </View>
                
        </ImageBackground>
        
    </View>


<Modal animationType="fade" transparent={true} visible={this.state.modalVisible ? true : false}>
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'rgba(0,0,0,0.67)'
                        }}>
                            <TouchableWithoutFeedback>
                                {this.renderModalContent()}
                            </TouchableWithoutFeedback>
                        </View>
                </Modal>

                {this.state.loading ?
    <View style={styles.loader}>
    <LinearGradient colors={color} style={{height:'100%',width:'100%'}}>
    <View style={{justifyContent: 'center',alignItems: 'center',height:'100%'}}> 
     <Animated.View style={animatedStyle}>
        <View  style={{height:70,width:70}}>
            <Image resizeMode='contain' style={{marginLeft:10,height:'100%',width:'88%'}} source={require('../Assets/playsav-mini-logo.png')}/>
        </View>    
      </Animated.View>
     <Text style={{alignSelf:'center',color:'white',top:35}}>Game Loading...</Text>
      </View>
      </LinearGradient>
   </View>
    : null
    }
 </View>
)
}


}

const styles = StyleSheet.create({
    loader: {
        // backgroundColor: 'rgba(210,227,233, 1.0)',
       // backgroundColor: 'white',#388FCB'

        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
       // flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignSelf: 'center',
        justifyContent: 'center',
    alignItems: 'center',
    }
});



