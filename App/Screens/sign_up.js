import React, {Component} from 'react';
import {Text, View, KeyboardAvoidingView, AsyncStorage, ActivityIndicator} from 'react-native';
import * as Animatable from 'react-native-animatable';
import TextBox from '../Components/text_box';
import Button from '../Components/button';
import Background from '../Components/background';
import SilverBox from '../Components/silver_box';
import TermsofServiceText from '../Components/terms_of_service_text';
import SilverBoxHeader from '../Components/silverbox_header';
import AuthFooter from '../Components/authscreen_footer';
import navigate from '../Components/navigate';
import api from '../api';
import firebase from 'react-native-firebase';

export default class SignUp extends Component {

    constructor() {
        super();
        this.state = {
            fname: '', lname: '', email: '', password: '', loading: false, error:'', errors:''
        }
        firebase.analytics().setCurrentScreen('Sign up');

    }

    componentDidMount() {
        this.refs.fname.slideInLeft();
        this.refs.lname.slideInRight();
        this.refs.password.fadeInUpBig();
    }

    isValid() {
        const { fname, email, password } = this.state;
        let valid = false;
        let reg = /\S+@\S+\.\S+/g ;
        let name_reg = /^[a-zA-Z].*/g;

        if (fname.length > 0 && email.length > 0 && password.length > 0) {

            if(name_reg.test(fname) === false) {
                this.setState({ error: 'Please enter a valid name.' });
            } else if(reg.test(email) === false) {
                this.setState({ error: 'Please enter a valid email address.' });
            } else if(password.length < 6 ) {
                this.setState({ error: 'Password must contain 6 characters.' });
            } else {
                this.setState({ error: '' });
                valid = true;
            }
        }

        if (fname.length === 0) {
            this.setState({ error: 'Please enter your name.' });
        } else if (email.length === 0) {
            this.setState({ error: 'Please enter an email address.' });
        } else if (password.length === 0) {
            this.setState({ error: 'Please enter a password.' });
        }

        return valid;
    }


    async register() {
        this.setState({error: '', errors:''});
        const { fname, lname, email, password } = this.state;
        this.setState({loading: true});
        let body = {
                "fname": fname,
                "lname": lname,
                "email": email,
                "password": password,
        };
        try {
            let response = await api.request('register', 'post', body, null);
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        AsyncStorage.multiSet(
                            [['user_details', JSON.stringify(data.user)],
                                ['access_token', data.access_token],
                                ['refresh_token', data.refresh_token]]
                        ).then(() => {
                            this.setState({loading: false});
                            AsyncStorage.setItem('isGuest','0')
                            navigate.navigateWithReset(this.props.navigation, 'InterestSlides')
                           // navigate.navigateWithReset(this.props.screenProps.rootNavigation, 'SignedIn')
                        })
                    });
                } catch (error) {
                    // console.log(error);
                    this.setState({loading: false});
                    alert("There was an error saving your data. Please register again.")
                }
            } else {
                await response.json().then((res) => {
                    this.setState({loading: false, errors: res.errors});
                });
            }
        } catch (error) {
            // console.log(error);
            this.setState({loading: false});
            alert("There was an error saving your data. Please register again.")
        }
    }

    signup_validate() {
        if (this.isValid()) {
            this.register();
        }
    };

    loginAsGuest = async ()=> {
    
        this.setState({loading: true});
        try {
            let response = await api.request('guest', 'post', null);
            if (response.status === 200) {
                console.log('Gust', JSON.stringify(response))
                try {
                    await response.json().then((data) => {
                        AsyncStorage.multiSet(
                            [['user_details', JSON.stringify(data.user)],
                                ['access_token', data.access_token],
                                ['refresh_token', data.refresh_token]]
                        ).then(() => {
                            this.setState({loading: false});
                            AsyncStorage.setItem('isMusic','1')
                            AsyncStorage.setItem('isGuest','1')
                             navigate.navigateWithReset(this.props.navigation, 'InterestSlides')
                        })
                    });
                } catch (error) {
                    console.log(error);
                    this.setState({loading: false});
                    alert("There was an error processing your data. Please login again." + response.status)
                }
            } else {
                await response.json().then((res) => {
                    this.setState({loading: false, error: res.error});
                });
            }
        } catch (error) {
            console.log(error);
            this.setState({loading: false});
            alert("There was an error processing your data. Please login again." + response.status)
        }
    }
    

    render() {
        return (
            <Background>
                <KeyboardAvoidingView behavior="position" enabled style={{alignItems: 'center'}}>
                    <SilverBox>
                        <SilverBoxHeader/>
                        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                            <Text style={{color: 'red'}}>
                                {this.state.error? this.state.error :this.state.errors}
                            </Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Animatable.View useNativeDriver ref={'fname'} style={{width: '50%'}}>
                                <TextBox placeholder={'First name'}
                                         width={'92%'}
                                         icon={require('../Assets/User.png')}
                                         autoCapitalize={'words'}
                                         onChangeText={(fname) => this.setState({fname})}
                                         onSubmitEditing={() => this.refs.lnametf.refs.lnametf.focus()}
                                />
                            </Animatable.View>
                            <Animatable.View useNativeDriver ref={'lname'} style={{width: '50%', alignItems: 'flex-end'}}>
                                <TextBox placeholder={'Last name'}
                                         width={'92%'}
                                         icon={require('../Assets/User.png')}
                                         autoCapitalize={'words'}
                                         reference={'lnametf'}
                                         ref={'lnametf'}
                                         onChangeText={(lname) => this.setState({lname})}
                                         onSubmitEditing={() => this.refs.email.refs.email.focus()}
                                />
                            </Animatable.View>
                        </View>
                        <TextBox placeholder={'Email'}
                                 icon={require('../Assets/mail.png')}
                                 keyboardType={'email-address'}
                                 reference={'email'}
                                 ref={'email'}
                                 onChangeText={(email) => this.setState({email})}
                                 onSubmitEditing={() => this.refs.passwordtf.refs.passwordtf.focus()}
                        />
                        {/* <Text style={{fontSize: 14, color: '#6A6A6A', marginTop: 10}}>
                            Please use your .edu email
                        </Text> */}
                        <Animatable.View useNativeDriver ref={'password'}>
                            <TextBox placeholder={'Password'}
                                     icon={require('../Assets/Passsword.png')}
                                     secureTextEntry
                                     returnKeyType={'done'}
                                     reference={'passwordtf'}
                                     ref={'passwordtf'}
                                     onChangeText={(password) => this.setState({password})}
                            />
                        </Animatable.View>
                        <TermsofServiceText/>
                    </SilverBox>
                </KeyboardAvoidingView>
                <View style={{position: 'absolute', alignSelf: 'center', bottom: 80}}>
                    <Button name={'SIGNUP'}
                            onPress={() => this.signup_validate()}
                            paddingHorizontal={80}/>
                            <View style={{ height: 20 }}/>

                            <Button name={'CONTINUE AS GUEST'}
                            onPress={() => this.loginAsGuest()}
                            paddingHorizontal={20}/>
                </View>
                
                <View style={{ marginTop: 50}}/>
                <AuthFooter signUp navigation={this.props.navigation}/>
                {this.state.loading &&
                <View style={{
                    flex: 1, position: 'absolute', left: 0,
                    right: 0, top: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color='rgb(54,122,223)' />
                </View>
                }
            </Background>
        );
    }
}