import React, {Component} from 'react';
import {
    Text,
    View,
    KeyboardAvoidingView,
    AsyncStorage,
    ActivityIndicator,
    TouchableOpacity,
    Modal,
    TouchableWithoutFeedback
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import TextBox from '../Components/text_box';
import Button from '../Components/button';
import Background from '../Components/background';
import SilverBox from '../Components/silver_box';
import TermsofServiceText from '../Components/terms_of_service_text';
import SilverBoxHeader from '../Components/silverbox_header';
import AuthFooter from '../Components/authscreen_footer';
import navigate from '../Components/navigate';
import api from '../api';
import firebase from 'react-native-firebase';
import { isIntro } from "../../App/auth";

export default class LogIn extends Component {

    constructor() {
        super();
        this.state = {
            email: '',
            password: '', 
            loading: false,
            error:'',
            errors:'',
            modalVisible: false,
            forgotEmail:'',
        }
        firebase.analytics().setCurrentScreen('Login');
    }
    componentWillMount(){
    }

    componentDidMount() {
        this.refs.email.slideInLeft();
        this.refs.password.slideInRight();
    }

    isValid() {
        const { email, password } = this.state;
        let valid = false;
        let reg = /\S+@\S+\.\S+/g ;

        if (email.length > 0 && password.length > 0) {

           if(reg.test(email) === false) {
                this.setState({ error: 'Please enter a valid email address.' });
            } else if(password.length < 6 ) {
                this.setState({ error: 'Password must contain 6 characters.' });
            } else {
                this.setState({ error: '' });
                valid = true;
            }
        }

        if (email.length === 0) {
            this.setState({ error: 'Please enter an email address.' });
        } else if (password.length === 0) {
            this.setState({ error: 'Please enter a password.' });
        }

        return valid;
    }

    login_validate() {
         if (this.isValid()) {
            this.login();
         }
    };


    async login() {
        this.setState({error: '', errors:''});
        const { email, password } = this.state;
        this.setState({loading: true});
        let body = {
            "email": email,
            "password": password,
        };
        try {
            let response = await api.request('login', 'post', body);
            if (response.status === 200) {
                try {
                    await response.json().then((data) => {
                        AsyncStorage.multiSet(
                            [['user_details', JSON.stringify(data.user)],
                                ['access_token', data.access_token],
                                ['refresh_token', data.refresh_token]]
                        ).then(() => {
                            this.setState({loading: false});
                            AsyncStorage.setItem('isMusic','1')
                            AsyncStorage.setItem('isGuest','0')
                            // navigate.navigateWithReset(this.props.screenProps.rootNavigation, 'SignedIn')
                             navigate.navigateWithReset(this.props.navigation, 'InterestSlides')

                        })
                    });
                } catch (error) {
                    console.log(error);
                    this.setState({loading: false});
                    alert("There was an error processing your data. Please login again." + response.status)
                }
            } else {
                await response.json().then((res) => {
                    // console.log(res);
                    this.setState({loading: false, error: res.error});
                });
            }
        } catch (error) {
            console.log(error);
            this.setState({loading: false});
            alert("There was an error processing your data. Please login again." + response.status)
        }
    }

forgotPassword = async () => {
    this.setState({error:''})
    let reg = /\S+@\S+\.\S+/g ;

    const {forgotEmail} = this.state

    if (forgotEmail.length > 0 ) {

        if(reg.test(forgotEmail) === false) {
             this.setState({ error: 'Please enter a valid email address.' });
         } else {
            this.setState({loading: true});
            let body = {
                "email": forgotEmail,
            };
            try {
                let response = await api.request('forgot-password', 'post', body);

                if (response.status === 200) {
                    this.setState({loading: false,modalVisible:false});
                    try {
                        await response.json().then((data) => {
                            alert(data.message)
                        });
                    } catch (error) {
                        console.log(error);
                        this.setState({loading: false});
                        alert("There was an error processing your data. Please try again." + response.status)
                    }
                } else {
                    await response.json().then((res) => {
                        this.setState({loading: false, error: res.error});
                    });
                }
            } catch (error) {
                console.log(error);
                this.setState({loading: false});
                alert("There was an error processing your data. Please try again." + response.status)
            }


         }
     }else{
        this.setState({ error: 'Please enter an email address.' });
    }
}


  loginAsGuest = async ()=> {
    

    this.setState({loading: true});
    try {
        let response = await api.request('guest', 'post', null);
        if (response.status === 200) {
            console.log('Gust', JSON.stringify(response))
            try {
                await response.json().then((data) => {
                    AsyncStorage.multiSet(
                        [['user_details', JSON.stringify(data.user)],
                            ['access_token', data.access_token],
                            ['refresh_token', data.refresh_token]]
                    ).then(() => {
                        this.setState({loading: false});
                        AsyncStorage.setItem('isMusic','1')
                        AsyncStorage.setItem('isGuest','1')
                         navigate.navigateWithReset(this.props.navigation, 'InterestSlides')
                    })
                });
            } catch (error) {
                console.log(error);
                this.setState({loading: false});
                alert("There was an error processing your data. Please login again." + response.status)
            }
        } else {
            await response.json().then((res) => {
                this.setState({loading: false, error: res.error});
            });
        }
    } catch (error) {
        console.log(error);
        this.setState({loading: false});
        alert("There was an error processing your data. Please login again." + response.status)
    }
}

    renderModalContent() {
        return(
            <KeyboardAvoidingView behavior="position" enabled>
            <View>
                <SilverBox style={{ alignSelf: 'center' }}>
                <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <View>
                <Text style={{fontSize: 16, color: '#000000', fontWeight: '600', marginVertical: 10, alignSelf: 'center'}}>
                Please enter your registered email to receive new password. </Text></View>
                <View>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                            <Text style={{color: 'red'}}>
                                {this.state.error? this.state.error :this.state.errors}
                            </Text>
                </View>

                {/*<KeyboardAvoidingView behavior="padding" enabled>*/}
                            <Animatable.View useNativeDriver ref={'email'}>
                                <TextBox placeholder={'Email'}
                                         icon={require('../Assets/mail.png')}
                                         keyboardType={'email-address'}
                                         onChangeText={(forgotEmail) => this.setState({forgotEmail})}
                                />
                            </Animatable.View>
                            {/*</KeyboardAvoidingView>*/}
                        </View>
                    <View style={{position: 'relative', alignSelf: 'center', zIndex: 3,top:40}}>
                    <Button name={'SEND'} paddingHorizontal={80}
                            onPress={() =>{ this.forgotPassword()}}/>
                </View>
                {this.state.loading &&
                <View style={{
                    flex: 1, position: 'absolute', left: 0,
                    right: 0, top: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color='rgb(54,122,223)' />
                </View>
                }
            </View>
                </SilverBox>
            </View>
            </KeyboardAvoidingView>
        )
    }

    render() {
        return (
            <Background>
                <KeyboardAvoidingView behavior="position" enabled style={{alignItems: 'center'}}>
                    <SilverBox>
                        <SilverBoxHeader/>
                        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                            <Text style={{color: 'red'}}>
                                {this.state.error? this.state.error :this.state.errors}
                            </Text>
                        </View>
                        <KeyboardAvoidingView behavior="padding" enabled>
                            <Animatable.View useNativeDriver ref={'email'}>
                                <TextBox placeholder={'Email'}
                                         icon={require('../Assets/mail.png')}
                                         keyboardType={'email-address'}
                                         onChangeText={(email) => this.setState({email})}
                                         onSubmitEditing={() => this.refs.passwordtf.refs.passwordtf.focus()}
                                />
                            </Animatable.View>
                            <Animatable.View useNativeDriver ref={'password'}>
                                <TextBox placeholder={'Password'}
                                         icon={require('../Assets/Passsword.png')}
                                         secureTextEntry
                                         returnKeyType={'done'}
                                         reference={'passwordtf'}
                                         ref={'passwordtf'}
                                         onChangeText={(password) => this.setState({password})}
                                />
                            </Animatable.View>
                        </KeyboardAvoidingView>
                        <TermsofServiceText/>
                    </SilverBox>
                </KeyboardAvoidingView>
                <View style={{position: 'absolute', alignSelf: 'center', zIndex: 3, bottom: 150}}>
                    <Button name={'LOGIN'} paddingHorizontal={80}
                            onPress={() => this.login_validate()}/>
                    <View style={{ height: 20 }}/>
                    <Button name={'CONTINUE AS GUEST'} paddingHorizontal={20}
                            onPress={() => this.loginAsGuest()}/>

                </View>

                <View style={{ marginTop: 30}}>
                <TouchableOpacity onPress={() => {this.setState({modalVisible:true,error:''})}}>
                <Text style={{color: 'white', fontSize: 14, marginTop: 100, alignSelf: 'flex-end', marginRight: 20}}>Forgot
                    password?</Text>
                    </TouchableOpacity>
                   </View> 
                <AuthFooter navigation={this.props.navigation}/>
                

{/* Forgot password Model */}
                <Modal animationType="fade" transparent={true} visible={this.state.modalVisible ? true : false}>
                    <TouchableWithoutFeedback onPress={() => {
                        this.setState({modalVisible: false,error:''})
                    }}>
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'rgba(0,0,0,0.67)'
                        }}>
                            <TouchableWithoutFeedback>
                                {this.renderModalContent()}
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {this.state.loading &&
                <View style={{
                    flex: 1, position: 'absolute', left: 0,
                    right: 0, top: 0, bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color='rgb(54,122,223)' />
                </View>
                }
            </Background>
        );
    }
}