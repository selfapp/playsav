import React, {Component} from 'react';
import {SafeAreaView, StatusBar, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import firebase from 'react-native-firebase';

import {createRootNavigator} from './App/router';
import {isSignedIn} from "./App/auth";

export default class App extends Component {

    constructor(props) {
        super(props);
        console.disableYellowBox = true;
        Text.defaultProps.allowFontScaling = false;

        this.state = {
            signedIn: false,
            checkedSignIn: false,
        }
    }

    componentDidMount() {
        firebase.analytics().setCurrentScreen('App');
    }

    componentWillUnmount() {
    }

    componentWillMount() {
        isSignedIn()
            .then(res => this.setState({signedIn: res, checkedSignIn: true}))
            .catch(err => alert(err));
    }


    render() {
        const {checkedSignIn, signedIn} = this.state;
        const Layout = createRootNavigator(signedIn);

        if (!checkedSignIn) {
            return null;
        }

        return (
            <LinearGradient colors={['silver', '#8DA9B7']} style={{flex: 1}}>
                <SafeAreaView style={{flex: 1}}>
                    <StatusBar backgroundColor="silver"/>
                    <Layout/>
                </SafeAreaView>
            </LinearGradient>
        );
    }
}
